import numpy as np
import pandas as pd
import time
import datetime
import os
import re


# ==============================================================================================
# check and clean data

def check_data(df):
    cr = np.array(df.CoresRequested)
    cu = np.array(df.NumberofAllocatedProcessors)
    wt = np.array(df.Runtime)
    rt = np.array(df.WallTimeRequested)

    x = np.mean(cr != cu)
    print(np.round(100*x, 2), "% of the jobs have a Number of Allocated Processors different of the number of requested processors")
    x = np.mean(cr < 0)
    print(np.round(100*x, 2), "% of the jobs have a number of requested cores smaller or equal to zero")
    x = np.mean(cu < 0)
    print(np.round(100*x, 2), "% of the jobs have a number of used cores smaller or equal to zero")
    x = np.mean(rt > wt)
    print(np.round(100*x, 2), "% of the jobs have a runtime higher than their walltime")
    x = np.mean(rt == 0)
    print(np.round(100*x, 2), "% of the jobs have a runtime equal to zero")
    x = np.mean(rt  < 0)
    print(np.round(100*x, 2), "% of the jobs have a runtime smaller than zero")

def clean_data(df):
    rt = np.array(df.Runtime)
    cond = rt > 0
    print('elimination of', len(cond) - cond.sum(), 'jobs with a negative or null runtime')
    df = df[cond]

    cr = np.array(df.CoresRequested)
    cond = cr>0
    print('elimination of', len(cond) - cond.sum(), 'jobs with a negative or null number of cores')
    df = df[cond]

    return df
#=====================================================================================
# load swf traces

def load_swf_trace(name):
    f = open(name, 'r')
    arr = f.readlines()
    f.close()

    arr = np.array(arr)
    cond = np.ones(arr.shape, dtype=bool)
    for i,e in enumerate(arr):
        if e[0] == ';':
            cond[i] = False
    arr = arr[cond]
    arr = [list(filter(None, e.split(' '))) for e in arr]
    for line in arr:
        line[-1] = line[-1][:-1]
    arr = np.array(np.round(np.array(arr, dtype = np.double), 0), dtype = int)

    cond = arr[:,7] == -1
    arr[:,7][cond] = arr[:,4][cond]

    cond = arr[:,4] == -1
    arr[:,4][cond] = arr[:,7][cond]

    dict_ = { 'UserID' : arr[:,11],
          'ProjectID' : arr[:,12],
          'QueueName' : arr[:,14],
          'NodesRequested' : arr[:,7],
          'CoresRequested' : arr[:,7],
          'WallTimeRequested' : arr[:,8],
          'QueuedTimestamp' : arr[:,1],
          'StartTime' : arr[:,1] + arr[:,2],
          'EndTime' : arr[:,1] + arr[:,2] + arr[:,3],
          'EligibleQueueTime' : -2*np.ones(len(arr)),
          'Runtime' : arr[:,3],
          'NodeSecondsUsed' : -2*np.ones(len(arr)),
          'CoreSecondsUsed' : -2*np.ones(len(arr)),
          'NumberofAllocatedProcessors' : arr[:,4]
        }

    res = pd.DataFrame(dict_, index = arr[:,0])
    check_data(res)
    return res

# =================================================================================================================
# load theta traces

def open_theta_traces(theta_path = 'traces/theta/'):
    df_list = []
    for file_name in os.listdir(theta_path):
      df_list.append(pd.read_csv(theta_path + file_name))
    # df = pd.concat(res_list)

    res_list = []
    for df in df_list:
      dict_ = { 'UserID' : np.array(df.USERNAME_GENID),
        'ProjectID' : np.array(df.PROJECT_NAME_GENID),
        'QueueName' : np.array(df.QUEUE_NAME),
        'NodesRequested' : np.array(df.NODES_REQUESTED),
        'CoresRequested' : np.array(df.CORES_REQUESTED),
        'WallTimeRequested' : np.array(df.WALLTIME_SECONDS),
        'QueuedTimestamp' : np.array(df.QUEUED_TIMESTAMP),
        'StartTime' : np.array(df.START_TIMESTAMP),
        'EndTime' : np.array(df.END_TIMESTAMP),
        'EligibleQueueTime' : -np.ones(len(df)),
        'Runtime' : np.array(df.RUNTIME_SECONDS),
        'NodeSecondsUsed' : np.array(df.NODES_USED)*np.array(df.RUNTIME_SECONDS),
        'CoreSecondsUsed' : np.array(df.USED_CORE_HOURS)*3600,
        'NumberofAllocatedProcessors' : np.array(df.CORES_USED)
      }   
      res = pd.DataFrame(dict_, index = range(len(df)))

      dates_list = ['QueuedTimestamp', 'StartTime', 'EndTime']
      for col in dates_list:
          res[col] = [ time.mktime(datetime.datetime.strptime(s, "%Y-%m-%d %H:%M:%S").timetuple()) for s in np.array(res[col])]
      res_list.append(res)


    res = pd.concat(res_list)
    res = res.sort_values(by=['QueuedTimestamp'])
    return res

#===================================================================
# load mira

def load_mira_traces(mira_path = 'traces/'):
  prefixs = ['Mira']*5
  suffix = ['2014','2015','2016','2017','2018']
  filenames = [mira_path + prefixs[i] + '/' + suffix[i] + '/job_trace.csv' for i in range(5)]

  df_list = []
  for filename in filenames:
      df_list.append(pd.read_csv(filename))

  def preprocess_data(df):
      df = df.rename(columns={"#NodesRequested": "NodesRequested", "#CoresRequested": "CoresRequested", " \t\t\t\t\t  StartTimestamp": "StartTime", 
      "EndTimestamp": "EndTime", "#NodeSecondsUsed": "NodeSecondsUsed", "#CoreSecondsUsed": "CoreSecondsUsed"})

      if type(df.QueuedTimestamp[0]) == str:
          alt = []
          for e in df.QueuedTimestamp:
              e_ = re.split('-| |:',e)
              e_ = [int(x) for x in e_[:-1]] + [int(e_[5].split('.')[0])]
              alt.append(datetime.datetime(e_[0], e_[1], e_[2], e_[3], e_[4], e_[5]).timestamp())
          df.QueuedTimestamp = alt

      if type(df.StartTime[0]) == str:
          alt = []
          for e in df.StartTime:
              e_ = re.split('-| |:',e)
              e_ = [int(x) for x in e_[:-1]] + [int(e_[5].split('.')[0])]
              alt.append(datetime.datetime(e_[0], e_[1], e_[2], e_[3], e_[4], e_[5]).timestamp())
          df.StartTime = alt

      if type(df.EndTime[0]) == str:
          alt = []
          for e in df.EndTime:
              e_ = re.split('-| |:',e)
              e_ = [int(x) for x in e_[:-1]] + [int(e_[5].split('.')[0])]
              alt.append(datetime.datetime(e_[0], e_[1], e_[2], e_[3], e_[4], e_[5]).timestamp())
          df.EndTime = alt
      return df

  for i,df in enumerate(df_list):
      df_list[i] = preprocess_data(df)

  df = pd.concat(df_list)
  df = df.sort_values(by=['QueuedTimestamp'])
  return df

#================================================================================================
# load intrepid traces

def load_intrepid_traces(intrepid_path = 'traces/'):
  prefixs = ['Intrepid']*5
  suffix = ['2009','2010','2011','2012','2013']
  filenames = [intrepid_path + prefixs[i] + '/' + suffix[i] + '/job_trace.csv' for i in range(5)]

  df_list = []
  for filename in filenames:
      df_list.append(pd.read_csv(filename))

  def preprocess_data(df):
      df = df.rename(columns={"#NodesRequested": "NodesRequested", "#CoresRequested": "CoresRequested", " \t\t\t\t\t  StartTimestamp": "StartTime", 
      "EndTimestamp": "EndTime", "#NodeSecondsUsed": "NodeSecondsUsed", "#CoreSecondsUsed": "CoreSecondsUsed"})

      if type(df.QueuedTimestamp[0]) == str:
          alt = []
          for e in df.QueuedTimestamp:
              e_ = re.split('-| |:',e)
              e_ = [int(x) for x in e_[:-1]] + [int(e_[5].split('.')[0])]
              alt.append(datetime.datetime(e_[0], e_[1], e_[2], e_[3], e_[4], e_[5]).timestamp())
          df.QueuedTimestamp = alt

      if type(df.StartTime[0]) == str:
          alt = []
          for e in df.StartTime:
              e_ = re.split('-| |:',e)
              e_ = [int(x) for x in e_[:-1]] + [int(e_[5].split('.')[0])]
              alt.append(datetime.datetime(e_[0], e_[1], e_[2], e_[3], e_[4], e_[5]).timestamp())
          df.StartTime = alt

      if type(df.EndTime[0]) == str:
          alt = []
          for e in df.EndTime:
              e_ = re.split('-| |:',e)
              e_ = [int(x) for x in e_[:-1]] + [int(e_[5].split('.')[0])]
              alt.append(datetime.datetime(e_[0], e_[1], e_[2], e_[3], e_[4], e_[5]).timestamp())
          df.EndTime = alt
      return df

  for i,df in enumerate(df_list):
      df_list[i] = preprocess_data(df)

  df = pd.concat(df_list)
  df = df.sort_values(by=['QueuedTimestamp'])
  return df

#===========================================================================================================

def put_batsim_output_in_Mira_format(job_list):
    job_list = [ jobs[jobs.final_state != 'REJECTED'] for jobs in job_list]
    res = []
    for df in job_list:
      dict_ = { 'UserID' : -np.ones(len(df)),
        'ProjectID' : -np.ones(len(df)),
        'QueueName' : -np.ones(len(df)),
        'NodesRequested' : np.array(df.requested_number_of_resources),
        'CoresRequested' : np.array(df.requested_number_of_resources),
        'WallTimeRequested' : np.array(df.requested_time),
        'QueuedTimestamp' : np.array(df.submission_time),
        'StartTime' : np.array(df.starting_time),
        'EndTime' : np.array(df.finish_time),
        'EligibleQueueTime' : -np.ones(len(df)),
        'Runtime' : np.array(df.execution_time),
        'NodeSecondsUsed' : np.array(df.requested_number_of_resources)*np.array(df.execution_time),
        'CoreSecondsUsed' : np.array(df.requested_number_of_resources)*3600,
        'NumberofAllocatedProcessors' : np.array(df.requested_number_of_resources)
      }   
      res.append(pd.DataFrame(dict_, index = range(len(df))))
    res = [e.sort_values(by=['QueuedTimestamp']) for e in res]
    return res