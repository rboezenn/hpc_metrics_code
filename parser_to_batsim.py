import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as ss
import pandas as pd
import shutil
import os

def make_profile(df,ind):
    d = df.iloc[ind]
    ch = '\"profile_' + str(ind) + '\" : {'
    ch += '\"type\": \"delay\", '
    ch += '\"runtime\" : ' + "{:.0f}".format(d.Runtime) + ', ' 
    ch += '\"delay\" : ' + "{:.0f}".format(d.Runtime) + ', ' 
    ch += '\"np\" : ' + "{:.0f}".format(d.NodesRequested) + ', '  
    ch += '\"command\" : ' + '\"????\"' + '}'
    return ch

def make_job(df, ind, min_subtime, time_list):
    d = df.iloc[ind]
    ch = ''
    pre = '    { \"id\" : '
    mid = ', \"profile\" : ' + '\"profile_' + str(ind) + '\", '
    mid += '\"res\" : ' + "{:.0f}".format(d.NodesRequested) + ', ' 
    mid += '\"subtime\" : ' + str( int(max(0, d.QueuedTimestamp - min_subtime) * 1) ) + ', '
    mid += '\"load_id\" : ' + str(ind) + ', '
    for i,t in enumerate(time_list):
        ch += pre + '\"' + str(ind) + '_' + str(i) + '_' + str(len(time_list)) + '\"' + mid 
        ch += '\"essai_numero\" : ' + str(i) + ', \"nb_times\" : ' + str(len(time_list)) + ', \"walltime\" : ' + "{:.0f}".format(t) + '}, \n'
    return ch

def workload_parseur(df, time_list_list):

    proportion_in_initiale_queue = 0             # Proportions of the jobs that are going to be used in the initial queue
    min_subtime = np.sort(np.array(df.QueuedTimestamp))[int(len(df)*proportion_in_initiale_queue)]

    # number_in_initiale_queue = 0            # Number of jobs that are going to be used in the initial queue
    # min_subtime = np.sort(np.array(df.QueuedTimestamp))[max(0,number_in_initiale_queue - 1)]

    ch = '{\n  \"description\" :  \"\", \n'
    ch += '  \"command\": \"\", \n'
    ch += '  \"date\": \"\", \n'
    ch += '  \"jobs\": [ \n'

    for ind in range(len(df)):
        ch += make_job(df, ind, min_subtime, time_list_list[ind])
    ch = ch[:-3]
    ch += '\n' + '  ],\n'

    ch += '  \"nb_res\": ' + str(49152) + ', \n  \"profiles\":\n  {\n'

    for ind in range(len(df) - 1):
        ch += '    ' + make_profile(df, ind) + ', \n'
    ch += '    ' + make_profile(df, len(df)-1) + '\n' + '  }\n}'

    return ch

#==================================================
# functions to change walltime
# meaning of arguments :
# x : walltime
# y : runtime, using it is cheating ;)

only_walltime = lambda x,y : np.array(x).reshape((len(x), 1))

only_runtime = lambda x,y : (np.array(y) + 1).reshape((len(y), 1))

ajusted_walltime = lambda x,y : np.maximum(np.array(x), np.array(y) + 1).reshape((len(x), 1))

def small_exact(x, y): # perfect prediction on small jobs and adjusted_walltime on the others
    cond = y < 1e3
    res = ajusted_walltime(x,y)
    res[cond] = only_runtime(x[cond], y[cond])
    return  res.reshape((len(x), 1))  

def medium_exact(x, y): # perfect prediction on medium jobs and adjusted_walltime on the others
    cond = np.logical_and(y > 1e3, y < 2e4)
    res = ajusted_walltime(x,y)
    res[cond] = only_runtime(x[cond], y[cond])
    return  res.reshape((len(x), 1))  

def big_exact(x, y): # perfect prediction on big jobs and adjusted_walltime on the others
    cond = y > 2e4
    res = ajusted_walltime(x,y)
    res[cond] = only_runtime(x[cond], y[cond])
    return  res.reshape((len(x), 1))  

# ================================================

def make_json(df, ind_min = 0, ind_max = None, name = 'test_workload.json', time_fct = only_walltime, time_list_list = None):

    df = df.sort_values(by=['QueuedTimestamp'])  

    df = df[ind_min:ind_max]
    
    if ind_max is None:
        ind_max = len(df)

    if time_list_list is None:
        wt = np.array(df.WallTimeRequested)
        rt = np.array(df.Runtime)
        time_list_list = time_fct(wt, rt)
    
    ch = workload_parseur(df, time_list_list)   

    with open(name, 'w') as f:
        f.write(ch)

def generates_batsim_instances_with_time_fct_list(df, name_list, time_fct_list, dir, ind_min = 0, ind_max = None):
    shutil.rmtree(dir)
    os.mkdir(dir)
    for i in range(len(name_list)):
        make_json(df, ind_min = ind_min, ind_max = ind_max, name = dir + name_list[i], time_fct = time_fct_list[i])

def generates_batsim_instances_with_time_fct_list_and_df_list(df_list, name_list, time_fct_list, dir, ind_min = 0, ind_max = None):
    shutil.rmtree(dir)
    os.mkdir(dir)
    for i in range(len(name_list)):
        make_json(df_list[i], ind_min = ind_min, ind_max = ind_max, name = dir + name_list[i], time_fct = time_fct_list[i])

def generates_batsim_instances_with_time_list_list_list(df, name_list, time_list_list_list, dir, ind_min = 0, ind_max = None):
    shutil.rmtree(dir)
    os.mkdir(dir)
    for i in range(len(name_list)):
        make_json(df, ind_min = ind_min, ind_max = ind_max, name = dir + name_list[i], time_list_list = time_list_list_list[i])

def generates_batsim_instances_with_tlll_and_df_list(df_list, name_list, time_list_list_list, dir, ind_min = 0, ind_max = None):
    shutil.rmtree(dir)
    os.mkdir(dir)
    for i in range(len(name_list)):
        make_json(df_list[i], ind_min = ind_min, ind_max = ind_max, name = dir + name_list[i], time_list_list = time_list_list_list[i])
