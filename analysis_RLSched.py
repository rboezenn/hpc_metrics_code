import numpy as np
import matplotlib.pyplot as plt
import os
import pandas as pd

import traces_analysis
import load_traces
import parser_to_batsim
import parser_from_batsim_results

def load_simulation_list(files_loc):  #to load simulations of the same size with different starts
    filenames = os.listdir(files_loc)
    _list = []
    for filename in filenames:
        _list.append(np.load(files_loc + filename))
    key = np.array([np.argmin(_list[ind][4] == -1.) for ind in range(len(_list))])
    order = np.argsort(key)
    _list = np.array(_list)[order]

    start_list = key[order]

    _list = list(_list)
    for i,elt in enumerate(_list):
        cut_min = np.argmin(elt[4] == -1.)
        cut_max = len(elt[4])  - np.argmax((elt[4] != -1.)[::-1])
        _list[i] = elt[:,cut_min:cut_max]

    df_list = []
    for i,elt in enumerate(_list):
        dict_ = { 'job_id' : np.array(elt[0,:], dtype = int),
          'submission_time' : elt[1,:],
          'requested_number_of_resources' : np.array(np.round(elt[6,:]), dtype = int),
          'requested_time' : elt[2,:],
          'starting_time' : elt[4,:],
          'execution_time' : elt[5,:],
          'finish_time' : elt[4,:] + elt[5,:],
          'waiting_time' : elt[4,:] - elt[1,:],
          'allocated_resources' : np.array(np.round(elt[6,:]), dtype = int),
          'final_state' : np.array(len(elt[0,:]) * ['COMPLETED_SUCCESSFULLY'])
        }

        df_list.append(pd.DataFrame(dict_, index = np.array(elt[0,:], dtype = int)))

    return _list, df_list, start_list

def weighted_quantiles(values, weights, quantiles=0.5):
    i = np.argsort(values)
    c = np.cumsum(weights[i])
    return values[i[np.searchsorted(c, np.array(quantiles) * c[-1])]]

def plot_behaviour_per_group_bis(df_list, x_parameter, xscale, num_groups, y_param, labels = None, yscale = 'linear', fontsize = 25):  
    # df_list is not the list df_u but rather [df_u[0], df_sd[0]]
    # !!! all dataframes must have jobs the same jobs regarding the x_parameter !!!
    rt = np.array(df_list[0].execution_time)
    cr = np.array(df_list[0].requested_number_of_resources)

    if x_parameter == 'area':
        xx = rt*cr
        plt.xlabel('Jobs Area')
    elif x_parameter == 'runtime':
        xx = rt
        plt.xlabel('Jobs runtime')
    elif x_parameter == 'cores':
        xx = cr
        plt.xlabel('Jobs ressources')
    else:
        print('parameter not implemented !')
        raise 'parameter not implemented !'
    
    if xscale == 'log':
        bins = np.geomspace( min(xx), max(xx), num = num_groups + 1)
        groups = np.sqrt(bins[1:] * bins[:1])
    elif xscale == 'linear':
        bins = np.linspace( min(xx), max(xx), num = num_groups + 1)
        groups = (bins[1:] + bins[:1])/2
    else:
        print('scale not implemented !')
        raise 'scale not implemented !'

    median = - np.ones(shape = (len(df_list), len(groups)))
    first_decile = - np.ones(shape = (len(df_list), len(groups)))
    last_decile = - np.ones(shape = (len(df_list), len(groups)))
    nb_of_elts = - np.ones(shape = (len(df_list), len(groups)))
    for i,df in enumerate(df_list):

        if y_param == 'wait time':
            yy = np.array(df.waiting_time)
            plt.ylabel('Wait time')
        elif y_param == 'response time':
            yy = np.array(df.waiting_time) + np.array(df.execution_time)
            plt.ylabel('Response time')
        elif y_param == 'slowdown':
            yy = (np.array(df.waiting_time) + np.array(df.execution_time))/np.array(df.execution_time)
            plt.ylabel('Slowdown')
        else:
            print('y_param not implemented !')
            raise 'y_param not implemented !'

        for j,gt in enumerate(groups):

            cond = np.logical_and(bins[j] < xx, xx <= bins[j+1])
            sample = yy[cond]
            nb_of_elts[i,j] = len(sample)
            median[i,j] = np.percentile(sample, 50)
            first_decile[i,j] = np.percentile(sample, 10) 
            last_decile[i,j] = np.percentile(sample, 90)

    colors = [u'#1f77b4', u'#ff7f0e', u'#2ca02c', u'#d62728', u'#9467bd', u'#8c564b', u'#e377c2', u'#7f7f7f', u'#bcbd22', u'#17becf']
    for i in range(len(df_list)):
        if labels != None:
            plt.plot(groups, median[i], label = labels[i])
        else:
            plt.plot(groups, median[i], c = colors[i])
        # for a,b,c in zip(groups, median[i], nb_of_elts[i]):    # to give number of lts in each group
        #     plt.text(a,b,int(c), fontsize = fontsize)

        # plt.plot(groups, first_decile[i], c = colors[i], ls = '--')  # to plot deciles as dotted lines
        # plt.plot(groups, last_decile[i], c = colors[i], ls = '--')


        plt.fill_between(groups, first_decile[i], last_decile[i], color = colors[i], alpha = 0.1)
        # plt.plot(groups, first_decile[i], linestyle = '--', c = colors[i],)
        # plt.plot(groups, last_decile[i], linestyle = '--', c = colors[i],)
    plt.grid(which = 'major', color = 'k')
    # plt.grid(which = 'minor')
    # plt.minorticks_on()
    plt.xscale(xscale)
    plt.yscale(yscale)
    plt.legend(fontsize = fontsize)
    plt.show()
    return median, first_decile, last_decile, nb_of_elts

def consecutive(data, stepsize=1):
    return np.split(data, np.where(np.diff(data) != stepsize)[0]+1)

def interpol_gantt_chart(df, nb_cores, round = None, figsize=(25,10), time_divider='seconds_to_days', x_max = 27):
    fig, ax = plt.subplots(figsize=figsize)
    ft = np.array(df.finish_time)
    cr = np.array(df.requested_number_of_resources, dtype = int)
    # ss = np.array(df.submission_time)
    st = np.array(df.starting_time)

    sorted_arguments = np.argsort(st)

    st = st[sorted_arguments]
    ft = ft[sorted_arguments]
    cr = cr[sorted_arguments]
    # ss = ss[sorted_arguments]

    if round is not None:
        ft.round(decimals=round)
        # cr.round(decimals=round)
        # ss.round(decimals=round)
        st.round(decimals=round)

    if time_divider == 'seconds_to_days':
        ft /= 3600*24
        st /= 3600*24
        plt.xlabel('Time (days)')
    plt.ylabel('Cores')

    min_start_time = np.amin(st)
    max_finish_time = np.amax(ft)
    # plt.xlim(left = 0, right = max_finish_time - min_start_time)
    plt.xlim(left = 0, right = x_max)
    plt.ylim(bottom = 0, top = nb_cores)

    timestamps = np.unique(np.concatenate((ft,st)))
    cores_used = -np.ones(shape=(len(timestamps), nb_cores), dtype = int)

    for i in range(len(cr)):

        position_of_concerned_timestamps = np.logical_and(st[i] <= timestamps, timestamps < ft[i] )
        concerned_timestamps = cores_used[position_of_concerned_timestamps]
        free_cores = np.all(concerned_timestamps == -1, axis = 0)

        ind_free_cores = np.argwhere(free_cores)[:,0]
        if len(ind_free_cores) < cr[i]:
            print('NOT ENOUGH CORES !!')
            raise 'NOT ENOUGH CORES !!'
        

        affected_cores = ind_free_cores[:cr[i]]

        cores_used[np.ix_(position_of_concerned_timestamps, affected_cores)] = i
 
        splitted_cores_list = consecutive(affected_cores)
        for ccores in splitted_cores_list:
            ax.add_patch(plt.Rectangle((st[i] - min_start_time, ccores[0]), ft[i] - st[i], len(ccores), edgecolor = 'red', facecolor = '#1f77b4' ))
    return cores_used

def load_simulation_list_bis(files_loc): #to load simulations of different length that start at 0
    filenames = os.listdir(files_loc)
    _list = []
    for filename in filenames:
        _list.append(np.load(files_loc + filename))
    key = np.array([ len(_list[ind][4]) - np.argmax((_list[ind][4] != -1.)[::-1]) for ind in range(len(_list))])
    # print(key)
    order = np.argsort(key)
    _list = np.array(_list)[order]

    start_list = key[order]

    _list = list(_list)
    for i,elt in enumerate(_list):
        cut_min = np.argmin(elt[4] == -1.)
        cut_max = len(elt[4])  - np.argmax((elt[4] != -1.)[::-1])
        _list[i] = elt[:,cut_min:cut_max]

    df_list = []
    for i,elt in enumerate(_list):
        dict_ = { 'job_id' : np.array(elt[0,:], dtype = int),
          'submission_time' : elt[1,:],
          'requested_number_of_resources' : np.array(np.round(elt[6,:]), dtype = int),
          'requested_time' : elt[2,:],
          'starting_time' : elt[4,:],
          'execution_time' : elt[5,:],
          'finish_time' : elt[4,:] + elt[5,:],
          'waiting_time' : elt[4,:] - elt[1,:],
          'allocated_resources' : np.array(np.round(elt[6,:]), dtype = int),
          'final_state' : np.array(len(elt[0,:]) * ['COMPLETED_SUCCESSFULLY'])}

        df_list.append(pd.DataFrame(dict_, index = np.array(elt[0,:], dtype = int)))

    return _list, df_list, start_list

def convert_batsim_format_to_mira_format(df):
    if type(df) == list:
        return [convert_batsim_format_to_mira_format(e) for e in df]
    else:
        dict_ = { 'job_id' : np.array(df.job_id, dtype = int),
        'QueuedTimestamp' : np.array(df.submission_time, dtype = int),
        'NodesRequested' : np.array(df.requested_number_of_resources, dtype = int),
        'WallTimeRequested' : np.array(df.requested_time, dtype = int),
        'Runtime' : np.array(df.execution_time, dtype = int)
        }
        return pd.DataFrame(dict_, index = np.array(df.job_id, dtype = int))

def create_batsim_instances(df_list, dir):
    df_list = convert_batsim_format_to_mira_format(df_list)
    # tlll = np.array([ np.array(e.WallTimeRequested, dtype = int) for e in df_list]).reshape( (len(df_list), len(df_list[0]), 1) )
    name_list = ['serie_' + str(i) + '.json' for i in range(len(df_list))]
    fct_list = [parser_to_batsim.ajusted_walltime]* len(name_list)
    parser_to_batsim.generates_batsim_instances_with_time_fct_list_and_df_list(df_list, name_list, fct_list, dir)


def set_box_color(bp, color):
    plt.setp(bp['boxes'], color=color)
    plt.setp(bp['whiskers'], color=color)
    plt.setp(bp['caps'], color=color)
    plt.setp(bp['medians'], color=color)

def boxplot_behaviour_per_group_bis(df_list, x_parameter, xscale, num_groups, y_param, labels = None, yscale = 'linear', fontsize = 25, x_scalse_mutltiplier = 1, y_scalse_mutltiplier = 1):  
    # df_list is not the list df_u but rather [df_u[0], df_sd[0]]
    # !!! all dataframes must have jobs the same jobs regarding the x_parameter !!!
    rt = np.array(df_list[0].execution_time)
    cr = np.array(df_list[0].requested_number_of_resources)

    if x_parameter == 'area':
        xx = rt*cr
        plt.xlabel('Jobs Area')
    elif x_parameter == 'runtime':
        xx = rt
        plt.xlabel('Jobs runtime')
    elif x_parameter == 'cores':
        xx = cr
        plt.xlabel('Jobs ressources')
    else:
        print('parameter not implemented !')
        raise 'parameter not implemented !'
    
    epsilon = 1e-3
    if xscale == 'log':
        bins = np.geomspace( min(xx)-epsilon, max(xx)+epsilon, num = num_groups + 1)
        groups = np.sqrt(bins[1:] * bins[:-1])
    elif xscale == 'linear':
        bins = np.linspace( min(xx), max(xx), num = num_groups + 1)
        groups = (bins[1:] + bins[:-1])/2
    else:
        print('scale not implemented !')
        raise 'scale not implemented !'

    yyy = []
    for i,df in enumerate(df_list):

        if y_param == 'wait time':
            yy = np.array(df.waiting_time)
            plt.ylabel('Wait time')
        elif y_param == 'response time':
            yy = np.array(df.waiting_time) + np.array(df.execution_time)
            plt.ylabel('Response time')
        elif y_param == 'slowdown':
            yy = (np.array(df.waiting_time) + np.array(df.execution_time))/np.array(df.execution_time)
            plt.ylabel('Slowdown')
        else:
            print('y_param not implemented !')
            raise 'y_param not implemented !'
        yy/= y_scalse_mutltiplier
        
        yyy.append([])
        for j,gt in enumerate(groups):

            cond = np.logical_and(bins[j] < xx, xx <= bins[j+1])
            yyy[i].append(yy[cond])
    groups /= x_scalse_mutltiplier
    colors = [u'#1f77b4', u'#ff7f0e', u'#2ca02c', u'#d62728', u'#9467bd', u'#8c564b', u'#e377c2', u'#7f7f7f', u'#bcbd22', u'#17becf']
    for i in range(len(df_list)):
        if xscale == 'log':
            pos_factor = groups[1]/groups[0]
            factor = pow(pos_factor, 1/(2*len(df_list)))
            relative_pos = [factor**(0.5+i-len(df_list)/2) for i in range(len(df_list))]
            a = plt.boxplot(yyy[i], notch = False, positions=groups*relative_pos[i], sym='', widths=groups*relative_pos[i]*pos_factor/40)
            plt.scatter(x=groups*relative_pos[i], y=[np.mean(a) for a in yyy[i]], c='k')
            plt.xlim(left = groups[0]*relative_pos[0]/np.sqrt(pos_factor), right = groups[-1]*relative_pos[-1]*np.sqrt(pos_factor))

        elif xscale == 'linear':
            pos_factor = groups[1] - groups[0]
            factor = pos_factor /( 2*len(df_list))
            relative_pos = [factor*(0.5+i-len(df_list)/2) for i in range(len(df_list))]
            a = plt.boxplot(yyy[i], notch = False, positions=groups+relative_pos[i], sym='', widths=pos_factor/(3*len(df_list)))
            plt.scatter(x=groups+relative_pos[i], y=[np.mean(a) for a in yyy[i]], c='k')
            plt.xlim(left = groups[0]+relative_pos[0]-pos_factor/2, right = groups[-1]+relative_pos[-1]+pos_factor/2)
    
        else:
            print('scale not implemented !')
            raise 'scale not implemented !'

        set_box_color(a, colors[i])
        if labels != None:
            plt.plot([], c=colors[i], label=labels[i])
    plt.ylabel('Wait time (hours)')
    plt.xlabel('Job area (cores-hours)')
    plt.grid(which = 'major', color = 'k')
    # plt.grid(which = 'minor')
    # plt.minorticks_on()
    plt.xscale(xscale)
    plt.yscale(yscale)
    plt.legend(fontsize = fontsize)
    
def write_df_in_swf(df, name):
    id = np.array(df.job_id, dtype = int)
    st = np.array(df.submission_time)
    st -= np.amin(st)
    st = np.array(st, dtype = int) + 1
    rt = np.array(df.execution_time, dtype = int)
    cr = np.array(df.requested_number_of_resources, dtype = int)
    wt = np.array(df.requested_time, dtype = int)

    epsilon = 1e-3
    max_id = int(np.log10(np.amax(id) + epsilon)) + 1
    max_st = len(str(np.amax(st)))
    max_rt = len(str(np.amax(rt)))
    max_cr = len(str(np.amax(cr)))
    max_wt = len(str(np.amax(wt)))

    lines = ''
    lines += '; MaxJobs: ' + str(len(df)) + '\n'
    lines += '; MaxRecords: ' + str(len(df)) + '\n'
    lines += '; MaxNodes: 256' + '\n'
    lines += '; MaxRuntime: 162754' + '\n'

    for i in range(len(df)):
        line = ''
        line += str(i+1) + (int(np.log10(len(df))) - int(np.log10(i+1 + epsilon)) ) * ' ' + ' '
        line += str(st[i]) + (max_st - len(str(st[i]))) * ' ' + ' '
        line += '-1 '
        line += str(rt[i]) + (max_rt - len(str(rt[i]))) * ' ' + ' '
        line += str(cr[i]) + (max_cr - len(str(cr[i]))) * ' ' + ' '
        line += '-1 '
        line += '-1 '
        line += '-1 '
        line += str(wt[i]) + (max_wt - len(str(wt[i]))) * ' ' + ' '
        line += '-1 '
        line += '1 '
        line += '-1 '
        line += '-1 '
        line += '-1 '
        line += '1 '
        line += '-1 '
        line += '-1 '
        line += '-1 '
        lines += line
        lines += '\n'
    f = open(name, "w")
    f.writelines(lines)
    f.close()   
