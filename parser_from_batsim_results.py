import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as ss
import pandas as pd
import shutil as shutil
import os
import seaborn as sns


# archive_dir_path = '/home/boezennec/Documents/code these/common-repository/batsim_archives/'
archive_dir_path = 'batsim_outputs/'

#========================================================================================

color_array = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']
directory = '/home/boezennec/Documents/batsim/output/'

def open_res(prefix = '', directory = directory):
    job_file_path =  directory + prefix + '_jobs.csv'
    schedule_file_path =  directory + prefix + '_schedule.csv'
    machine_states_file_path =  directory + prefix + '_machine_states.csv'
    jobs = pd.read_csv(job_file_path)
    machine_states = pd.read_csv(machine_states_file_path)
    schedule = pd.read_csv(schedule_file_path)
    return jobs, schedule, machine_states 

def open_jobs(prefix = '', directory = directory):
    job_file_path =  directory + prefix + '_jobs.csv'
    jobs = pd.read_csv(job_file_path)
    return jobs

from matplotlib.patches import Rectangle
def plot_gantt_chart(jobs, total_number_of_nodes=49152, show_job_numbers = False):
    cond = np.array(jobs.final_state != 'REJECTED')
    jobs = jobs[cond]
    if show_job_numbers :
        jobs = jobs.sort_values(by=['submission_time'])
    start_time = np.array(jobs.starting_time)
    runtime = np.array(jobs.execution_time)
    arr = np.array(jobs.allocated_resources)
    xmin = min(np.array(jobs.submission_time))
    xmax = max(np.array(jobs.finish_time))
    
    cst = 3600*24
    start_time, runtime, xmin, xmax = start_time/cst, runtime/cst, xmin/cst, xmax/cst # Pour mettre le temps en jours

    nb_essai = np.array([int(e.split('_')[1]) for e in np.array(jobs.job_id)])
    essai_max = np.array([int(e.split('_')[2]) for e in np.array(jobs.job_id)])
    fig, ax = plt.subplots(figsize=(20,10))
    
    ss = [ e.split(' ') for e in arr]
    ss = [[ ee.split('-') for ee in e] for e in ss]
    aa = [[ int(e[0]) for e in ee] for ee in ss]
    bb = [[ int(e[-1]) for e in ee] for ee in ss]

    cond1 = np.logical_and(nb_essai + 1 == essai_max, essai_max > 1) # In blue : The ones that are at their last tries
    cond2 = np.logical_and(nb_essai + 1 < essai_max, jobs.final_state == 'COMPLETED_WALLTIME_REACHED') # In orange : try a time smaller than Wt but failed
    cond3 = np.logical_and(nb_essai + 1 < essai_max, jobs.final_state == 'COMPLETED_SUCCESSFULLY') # In green : try a time smaller than Wt and succes
    # So : purpule = the ones launched again
    color_ind = 4 * np.ones(len(jobs), dtype =int)
    color_ind[cond1] = 0
    color_ind[cond2] = 1
    color_ind[cond3] = 2
    edge_color_arr = ['red', '#ff7f0e', '#2ca02c','red','red']
    if any(np.logical_and(cond1, cond2)): 
        raise 'erreur 12 dans plot_gantt_chart'
    if any(np.logical_and(cond2, cond3)):
        raise 'erreur 23 dans plot_gantt_chart'
    if any(np.logical_and(cond1, cond3)):
        raise 'erreur 13 dans plot_gantt_chart'
    for i in range(len(jobs)):

        for a,b in zip(aa[i], bb[i]):
            
            # ax.add_patch(Rectangle((start_time[i], a), runtime[i], b - a, edgecolor = edge_color_arr[color_ind[i]], facecolor = color_array[color_ind[i]] ))
            # ax.add_patch(Rectangle((start_time[i], a), runtime[i], b - a, edgecolor = 'red', facecolor = color_array[color_ind[i]] ))

            ax.add_patch(Rectangle((start_time[i], a), runtime[i], b - a, edgecolor = 'red', facecolor = '#1f77b4' ))
            
            if show_job_numbers :
                plt.text( start_time[i]+runtime[i]/2, (a+b)/2, str(i+1), fontsize = 30)

    ymin = 0
    ymax = total_number_of_nodes
    ax.set(xlim=(xmin, xmax), ylim=(ymin, ymax))
    plt.xlabel('time')
    plt.ylabel('ressources')
    plt.show()

def user_loss(jobs, weight_for_size = True, use_asked_workload = False):
    time = np.array(jobs.finish_time - jobs.submission_time)
    if not weight_for_size:
        return time.mean()
    if use_asked_workload:
        runtime = np.array(jobs.requested_time)
    else:
        runtime = np.array(jobs.execution_time)
    nodes = np.array(jobs.requested_number_of_resources)
    rn = runtime*nodes
    return (time*rn).sum()/rn.sum()

def wait_time(jobs, weight_for_size = True, use_asked_workload = False):
    time = np.array(jobs.starting_time - jobs.submission_time)
    if not weight_for_size:
        return time.mean()
    if use_asked_workload:
        runtime = np.array(jobs.requested_time)
    else:
        runtime = np.array(jobs.execution_time)
    nodes = np.array(jobs.requested_number_of_resources)
    rn = runtime*nodes
    return (time*rn).sum()/rn.sum()

def utilization(jobs, total_number_of_nodes=49152):
    max_time_nodes = (max(np.array(jobs.finish_time)) - min(np.array(jobs.starting_time))) * total_number_of_nodes
    return (np.array(jobs.execution_time)*np.array(jobs.requested_number_of_resources)).sum() / max_time_nodes

def clear_useless_jobs(jobs):
    cond = np.zeros(len(jobs), dtype=bool)
    cond[np.array(jobs.final_state == 'COMPLETED_SUCCESSFULLY')] = True
    ch_l = list(jobs.job_id)
    ch_l = [e.split('_') for e in ch_l]
    cond1 = np.array([int(e[1]) + 1 == int(e[2]) for e in ch_l])
    cond[cond1] = True
    cond[np.array(jobs.final_state == 'REJECTED')] = False

    # cond = np.zeros(len(jobs), dtype=bool)
    # cond[np.array(jobs.final_state == 'COMPLETED_SUCCESSFULLY')] = True
    # cond[np.array(jobs.final_state == 'REJECTED')] = False
    # cond[np.array(jobs.final_state == 'COMPLETED_WALLTIME_REACHED')] = False
    # print(len(cond), cond.sum())
    return jobs[cond]

def user_loss_with_dropout(jobs, weight_for_size = True, use_asked_workload = False, dropout_ratio = [0.15, 0.85]):
    qt = np.array(jobs.submission_time)
    cut_min = np.percentile(qt, 100 * dropout_ratio[0])
    cut_max = np.percentile(qt, 100 * dropout_ratio[1])
    cond = np.logical_and(qt >= cut_min, qt <= cut_max)

    time = np.array(jobs.finish_time - jobs.submission_time)[cond]
    if not weight_for_size:
        return time.mean()
    if use_asked_workload:
        runtime = np.array(jobs.requested_time)[cond]
    else:
        runtime = np.array(jobs.execution_time)[cond]
    nodes = np.array(jobs.requested_number_of_resources)[cond]
    rn = runtime*nodes
    return (time*rn).sum()/rn.sum()

def user_loss_weighted_per_core_with_dropout(jobs, dropout_ratio = [0.15, 0.85]):
    qt = np.array(jobs.submission_time)
    cut_min = np.percentile(qt, 100 * dropout_ratio[0])
    cut_max = np.percentile(qt, 100 * dropout_ratio[1])
    cond = np.logical_and(qt >= cut_min, qt <= cut_max)   

    time = np.array(jobs.finish_time - jobs.submission_time)[cond]
    nodes = np.array(jobs.requested_number_of_resources)[cond]
    return (time*nodes).sum()/nodes.sum()

def user_loss_weighted_per_runtime_with_dropout(jobs, dropout_ratio = [0.15, 0.85]):
    qt = np.array(jobs.submission_time)
    cut_min = np.percentile(qt, 100 * dropout_ratio[0])
    cut_max = np.percentile(qt, 100 * dropout_ratio[1])
    cond = np.logical_and(qt >= cut_min, qt <= cut_max)   

    time = np.array(jobs.finish_time - jobs.submission_time)[cond]
    runtime = np.array(jobs.execution_time)[cond]
    return (time*runtime).sum()/runtime.sum()

def bounded_slowndown(jobs, dropout_ratio = [0.15,0.85], weight_for_size = True, use_asked_workload = False):
    qt = np.array(jobs.submission_time)
    cut_min = np.percentile(qt, 100 * dropout_ratio[0])
    cut_max = np.percentile(qt, 100 * dropout_ratio[1])
    cond = np.logical_and(qt >= cut_min, qt <= cut_max)

    runtime = np.array(jobs.finish_time - jobs.starting_time)[cond]
    wait_time = np.array(jobs.starting_time - jobs.submission_time)[cond]

    tau = 10
    sd = np.maximum(1, (wait_time+runtime)/np.maximum(tau,runtime) )

    if not weight_for_size:
        return sd.mean()
    if use_asked_workload:
        weights = np.array(jobs.requested_time)[cond] * np.array(jobs.requested_number_of_resources)[cond]
    else:
        weights = np.array(jobs.execution_time)[cond] * np.array(jobs.requested_number_of_resources)[cond]

    return (sd*weights).sum()/weights.sum()

def max_bounded_slowndown(jobs, dropout_ratio = [0.15,0.85]):
    qt = np.array(jobs.submission_time)
    cut_min = np.percentile(qt, 100 * dropout_ratio[0])
    cut_max = np.percentile(qt, 100 * dropout_ratio[1])
    cond = np.logical_and(qt >= cut_min, qt <= cut_max)

    runtime = np.array(jobs.finish_time - jobs.starting_time)[cond]
    wait_time = np.array(jobs.starting_time - jobs.submission_time)[cond]

    tau = 10 # in seconds
    sd = np.maximum(1, (wait_time+runtime)/np.maximum(tau,runtime) )
    return np.max(sd)

def utilization_with_dropout(jobs, dropout_ratio = [0.15,0.85], total_number_of_nodes=49152):
    ft = np.array(jobs.finish_time)
    st = np.array(jobs.starting_time)
    cr = np.array(jobs.requested_number_of_resources)
    qt = np.array(jobs.submission_time)
    qtm = np.amin(qt)
    qt -= qtm
    st -= qtm
    ft -= qtm

    min_cut_time = np.percentile(qt, 100 * dropout_ratio[0])
    max_cut_time = np.percentile(qt, 100 * dropout_ratio[1]) 
    
    cond = np.logical_and(st <= max_cut_time, ft >= min_cut_time)
    ft, st, cr = ft[cond], st[cond], cr[cond]
    max_time_nodes = (max_cut_time - min_cut_time) * total_number_of_nodes
    return ((np.minimum(ft, max_cut_time) - np.maximum(st, min_cut_time)) * cr).sum() / max_time_nodes

def number_of_cores_used(df):
    start = np.array(df.starting_time)
    end = np.array(df.finish_time)
    cores_requested = np.array(df.requested_number_of_resources)
    start_arg = np.argsort(np.array(start))
    end_arg = np.argsort(np.array(end))

    si, ei = 0, 0
    times, cores_used = [min(start)], [0]

    while max(si, ei) < len(df):
        if start[start_arg[si]] < end[end_arg[ei]]:
            times.append(start[start_arg[si]])
            cores_used+= [cores_used[-1] + cores_requested[start_arg[si]]]
            si += 1
        else:
            times.append(end[end_arg[ei]])
            cores_used+= [cores_used[-1] - cores_requested[end_arg[ei]]]
            ei += 1
    while si < len(df):
            times.append(start[start_arg[si]])
            cores_used+= [cores_used[-1] + cores_requested[start_arg[si]]]
            si += 1
    while ei < len(df):
            times.append(end[end_arg[ei]])
            cores_used+= [cores_used[-1] - cores_requested[end_arg[ei]]]
            ei += 1 
    return np.array(times), np.array(cores_used)

def utilization_std(jobs, total_number_of_nodes=49152, dropout_ratio = [0.15,0.85]):

    # qt = np.array(jobs.submission_time)
    # cut_min = np.percentile(qt, 100 * dropout_ratio[0])
    # cut_max = np.percentile(qt, 100 * dropout_ratio[1])
    # cond = np.logical_and(qt > cut_min, qt < cut_max)   
    # jobs = jobs[cond]

    t,c = number_of_cores_used(jobs)
    values = c[:-1]
    weights = t[1:] - t[:-1]

    qt = np.array(jobs.submission_time)
    cut_min = np.percentile(qt, 100 * dropout_ratio[0])
    cut_max = np.percentile(qt, 100 * dropout_ratio[1])
    cond = np.logical_and(t[:-1] >= cut_min, t[:-1] <= cut_max)
    
    weights = weights[cond]
    values = values[cond]

    average = np.average(values, weights=weights)
    variance = np.average((values-average)**2, weights=weights)
    return np.sqrt(variance)/total_number_of_nodes

def utilization_moment(jobs, alpha = 2, total_number_of_nodes=49152, dropout_ratio = [0.15,0.85]):

    t,c = number_of_cores_used(jobs)
    values = c[:-1]
    weights = t[1:] - t[:-1]

    qt = np.array(jobs.submission_time)
    cut_min = np.percentile(qt, 100 * dropout_ratio[0])
    cut_max = np.percentile(qt, 100 * dropout_ratio[1])
    cond = np.logical_and(t[:-1] >= cut_min, t[:-1] <= cut_max)  

    weights = weights[cond]
    values = values[cond]/total_number_of_nodes

    return np.average(values**alpha, weights=weights)
    
    
def print_loss_without_dropout(jobs, total_number_of_nodes = 49152):
    cond = np.array(jobs.final_state != 'REJECTED')
    jobs = jobs[cond]
    print('                           raw utilization :', "{:.5f}".format(utilization(jobs, total_number_of_nodes = total_number_of_nodes)))
    jobs = clear_useless_jobs(jobs)
    print('                        useful utilization :', "{:.5f}".format(utilization(jobs, total_number_of_nodes = total_number_of_nodes)))
    print('                         raw response time :', "{:.1f}".format(user_loss(jobs, False)))
    print('response time (weighted by asked workload) :', "{:.1f}".format(user_loss(jobs, use_asked_workload = True)))
    print(' response time (weighted by area) :', "{:.1f}".format(user_loss(jobs)))

def print_loss_with_dropout(jobs, dropout_ratio = [0.15,0.85], total_number_of_nodes =49152):
    cond = np.array(jobs.final_state != 'REJECTED')
    jobs = jobs[cond]
    print('            raw utilization (with dropout) :', "{:.5f}".format(utilization_with_dropout(jobs, dropout_ratio = dropout_ratio, total_number_of_nodes = total_number_of_nodes)))
    jobs = clear_useless_jobs(jobs)
    print('         useful utilization (with dropout) :', "{:.5f}".format(utilization_with_dropout(jobs, dropout_ratio = dropout_ratio, total_number_of_nodes = total_number_of_nodes)))
    print('                         raw response time :', "{:.1f}".format(user_loss_with_dropout(jobs, False, dropout_ratio = dropout_ratio)))
    print('response time (weighted by asked workload) :', "{:.1f}".format(user_loss_with_dropout(jobs, use_asked_workload = True, dropout_ratio = dropout_ratio)))
    print(' response time (weighted by area) :', "{:.1f}".format(user_loss_with_dropout(jobs, dropout_ratio = dropout_ratio)))

def print_loss(jobs, print_without_dropout = True, print_with_dropout = True, dropout_ratio = [0.15,0.85], total_number_of_nodes = 49152):
    print('jobs :', len(jobs), ', rejected :', np.array(jobs.final_state == 'REJECTED').sum(), ', killed :',
    np.array(jobs.final_state == 'COMPLETED_WALLTIME_REACHED').sum(),    ', completed :', np.array(jobs.final_state == 'COMPLETED_SUCCESSFULLY').sum())
    print('                              duration :', int(max(np.array(jobs.finish_time)) - min(np.array(jobs.starting_time))))
    if print_without_dropout:
        print('with raw data : -------------------------------')
        print_loss_without_dropout(jobs, total_number_of_nodes = total_number_of_nodes)
    if print_with_dropout:
        print('with dropout : --------------------------------')
        print_loss_with_dropout(jobs, dropout_ratio = dropout_ratio, total_number_of_nodes = total_number_of_nodes)

# def return_jobs_without_droupout(res, jobs):
#     cond = np.array(jobs.final_state != 'REJECTED')
#     jobs = jobs[cond]
#     res['raw utilization'] = utilization(jobs)
#     jobs = clear_useless_jobs(jobs)
#     res['useful utilization'] = utilization(jobs)
#     res['raw response time'] =  user_loss(jobs, False)
#     res['response time (weighted by asked workload)'] = user_loss(jobs, use_asked_workload = True)
#     res['response time (weighted by area)'] = user_loss(jobs)

def return_jobs_with_droupout(res, jobs, dropout_ratio, total_number_of_nodes = 49152):
    cond = np.array(jobs.final_state != 'REJECTED')
    jobs = jobs[cond]
    res['raw utilization'] = utilization_with_dropout(jobs, dropout_ratio = dropout_ratio, total_number_of_nodes = total_number_of_nodes)
    res['utilization_std'] = utilization_std(jobs, total_number_of_nodes=total_number_of_nodes, dropout_ratio = dropout_ratio)
    res['utilization moment'] = utilization_moment(jobs, alpha=2, total_number_of_nodes=total_number_of_nodes, dropout_ratio = dropout_ratio )
    # jobs = clear_useless_jobs(jobs)
    res['useful utilization'] = utilization_with_dropout(jobs, dropout_ratio = dropout_ratio, total_number_of_nodes = total_number_of_nodes)
    res['boundless utilization'] = utilization(jobs, total_number_of_nodes = total_number_of_nodes)
    res['raw response time'] = user_loss_with_dropout(jobs, weight_for_size = False, dropout_ratio = dropout_ratio)
    res['response time (weighted by asked workload)'] = user_loss_with_dropout(jobs, weight_for_size = True, use_asked_workload = True, dropout_ratio = dropout_ratio)
    res['response time (weighted by area)'] = user_loss_with_dropout(jobs, weight_for_size = True, use_asked_workload = False, dropout_ratio = dropout_ratio)
    res['response time (weighted by number of cores)'] = user_loss_weighted_per_core_with_dropout(jobs, dropout_ratio = dropout_ratio)
    res['response time (weighted by execution time)'] = user_loss_weighted_per_runtime_with_dropout(jobs, dropout_ratio = dropout_ratio)
    res['raw bounded slowdown'] = bounded_slowndown(jobs, dropout_ratio = dropout_ratio, weight_for_size = False, use_asked_workload = False)
    res['bounded slowdown (weighted by asked workload)'] = bounded_slowndown(jobs, dropout_ratio = dropout_ratio, weight_for_size = True, use_asked_workload = True)
    res['bounded slowdown (weighted by area)'] = bounded_slowndown(jobs, dropout_ratio = dropout_ratio, weight_for_size = True, use_asked_workload = False)
    res['max bounded slowdown'] = max_bounded_slowndown(jobs, dropout_ratio = dropout_ratio)

def return_loss(jobs, dropout_ratio = [0.15,0.85], total_number_of_nodes = 49152):
    res = {}
    res['nb_of_jobs'] = len(jobs)
    res['nb_rejected'] = np.array(jobs.final_state == 'REJECTED').sum()
    res['nb_killed'] = np.array(jobs.final_state == 'COMPLETED_WALLTIME_REACHED').sum()
    res['nb_completed'] = np.array(jobs.final_state == 'COMPLETED_SUCCESSFULLY').sum()
    res['duration'] = int(np.nanmax(np.array(jobs.finish_time)) - np.nanmin(np.array(jobs.starting_time)))
    # if return_without_dropout:
    #     return_jobs_without_droupout(res, jobs)
    # if return_with_dropout:
    #     return_jobs_with_droupout(res, jobs, dropout_ratio)
    return_jobs_with_droupout(res, jobs, dropout_ratio, total_number_of_nodes = total_number_of_nodes)
    return res

batsim_out_dir = '/home/boezennec/Documents/batsim/output/'
archive_out_dir = '/home/boezennec/Documents/common-repository/batsim_archives/'
def move_results_in_archive_output(expe_name, readme = None):
    if os.path.isdir(archive_out_dir + expe_name):
        raise 'folder already existing'

    os.mkdir(archive_out_dir + expe_name)

    if readme is not None:
        with open(archive_out_dir + expe_name + '/readme.txt', 'w') as f:
            f.write(readme)
            f.close()

    for d in os.listdir(batsim_out_dir):
        if os.path.isdir(batsim_out_dir + d):
            os.mkdir(archive_out_dir + expe_name + '/' + d)
            for e in ['_jobs.csv', '_machine_states.csv', '_schedule.csv']:
                shutil.copyfile(batsim_out_dir + d + '/' + e, archive_out_dir + expe_name + '/' + d + '/' + e)
            shutil.rmtree(batsim_out_dir + d)


def make_jobs_list(expe_name, archive_dir_path = archive_dir_path):
    jobs_list = []
    name_list = []
    for folder in os.listdir(archive_dir_path + expe_name):
        a, _, _ = open_res(directory = archive_dir_path + expe_name + '/' + folder + '/')
        jobs_list.append(a)
        name_list.append(folder)

    order = np.argsort(name_list)
    jobs_list = [ jobs_list[e] for e in order]
    name_list = [ name_list[e] for e in order]
    return jobs_list, name_list

def make_loss_list(jobs_list, total_number_of_nodes = 49152, dropout = [0.15, 0.85]):
    dict_list = []
    for job in jobs_list:
        dict_list.append(return_loss(job, dropout_ratio = dropout, total_number_of_nodes = total_number_of_nodes))
    overall_dict = {}
    for key in dict_list[0]:
        l = [d[key] for d in dict_list]
        overall_dict[key] = l
    return overall_dict

# def make_loss_list_per_core(jobs_list):
#     dict_list = []
#     for job in jobs_list:
#         dict_list.append(return_loss(job, dropout_ratio = [0.15,0.85]))
#     overall_dict = {}
#     for key in dict_list[0]:
#         l = [d[key] for d in dict_list]
#         overall_dict[key] = l
#     return core_list, 

#=========================================================================
# Loss per group

def user_loss_with_groups(jobs, weight_for_size = True, use_asked_workload = False):
    
    time = np.array(jobs.finish_time - jobs.submission_time)
    if not weight_for_size:
        return time.mean()
    if use_asked_workload:
        runtime = np.array(jobs.requested_time)
    else:
        runtime = np.array(jobs.execution_time)
    nodes = np.array(jobs.requested_number_of_resources)
    rn = runtime*nodes
    return (time*rn).sum()/rn.sum()

def bounded_slowdown_with_groups(jobs, weight_for_size = True, use_asked_workload = False):

    runtime = np.array(jobs.finish_time - jobs.starting_time)
    wait_time = np.array(jobs.starting_time - jobs.submission_time)
    sd = np.maximum(1, (wait_time+runtime)/np.maximum(10,runtime) )
    if not weight_for_size:
        return sd.mean()
    if use_asked_workload:
        weights = np.array(jobs.requested_time) * np.array(jobs.requested_number_of_resources)
    else:
        weights = np.array(jobs.execution_time) * np.array(jobs.requested_number_of_resources)

    return (sd*weights).sum()/weights.sum()

# Caution : the elements of jobs_list1 and jobs_list2 must be in the same order:
# - the dict will first put all the jobs from L1 and then from L2
# - the i-th job of L1 must correspond to the i-th job of L2
def make_pair_loss(jobs_list1, jobs_list2, criterion, bins = None, n=12, dropout_ratio = [0.15,0.85]):

    jobs_list1 = [clear_useless_jobs(jobs) for jobs in jobs_list1]
    jobs_list1 = [jobs.sort_values(by=['job_id']) for jobs in jobs_list1]
    jobs_list2 = [clear_useless_jobs(jobs) for jobs in jobs_list2]
    jobs_list2 = [jobs.sort_values(by=['job_id']) for jobs in jobs_list2]

    if criterion == 'nb res':
        val = np.concatenate([np.array(jobs.requested_number_of_resources) for jobs in jobs_list1])
    elif criterion == 'execution time':
        val = np.concatenate([np.array(jobs.execution_time) for jobs in jobs_list1])
    elif criterion == 'area':
        val = np.concatenate([np.array(jobs.requested_number_of_resources) for jobs in jobs_list1]) * np.concatenate([np.array(jobs.execution_time) for jobs in jobs_list1])
    elif criterion == 'asked workload':
        val = np.concatenate([np.array(jobs.requested_number_of_resources, dtype=np.double) for jobs in jobs_list1])
        val1 = np.concatenate([np.array(jobs.requested_time) for jobs in jobs_list1])
        val2 = np.concatenate([np.array(jobs.requested_time) for jobs in jobs_list2])
        val= np.maximum(val1, val2)
    elif criterion == 'requested time':
        val1 = np.concatenate([np.array(jobs.requested_time) for jobs in jobs_list1])
        val2 = np.concatenate([np.array(jobs.requested_time) for jobs in jobs_list2])
        val = np.maximum(val1, val2)
    elif criterion == 'rt/wt':
        val = np.concatenate([np.array(jobs.execution_time) for jobs in jobs_list1])
        val1 = np.concatenate([np.array(jobs.requested_time) for jobs in jobs_list1])
        val2 = np.concatenate([np.array(jobs.requested_time) for jobs in jobs_list2])
        val/= np.maximum(val1, val2)
    else:
        print('erreur 63 !!!')
        raise 'erreur 63 !!!'

    if bins == None:
        bins = np.linspace(np.min(val), np.max(val), n)
        mean_values = (bins[1:] + bins[:-1])/2
    elif bins == 'lin':
        bins = np.linspace(np.min(val), np.max(val), n)
        mean_values = (bins[1:] + bins[:-1])/2
    elif bins == 'pow2':
        bins = np.round(np.geomspace(16, 2**17, 14) * 0.9, n)
        mean_values = np.round(np.sqrt(bins[:-1]*bins[1:]), 1)  
    elif bins == 'geom':
        bins = np.geomspace(np.min(val), np.max(val), n)
        mean_values = np.round(np.sqrt(bins[:-1]*bins[1:]), 1)       
    else:
        bins = np.array(bins)
        mean_values = np.array(mean_values)

    loss_list = ['nb_of_jobs', 'nb_rejected', 'nb_killed', 'nb_completed', 'duration', 'raw response time', 'response time (weighted by asked workload)', 'response time (weighted by area)', 'raw bounded slowdown', 'bounded slowdown (weighted by asked workload)', 'bounded slowdown (weighted by area)']

    res = {}
    for loss_name in loss_list:
        for mean_value in mean_values:
            res[loss_name, mean_value] = []

    # To only display it for certains values of rt/wt
    # __val = np.concatenate([np.array(jobs.execution_time) for jobs in jobs_list1])
    # __val1 = np.concatenate([np.array(jobs.requested_time) for jobs in jobs_list1])
    # __val2 = np.concatenate([np.array(jobs.requested_time) for jobs in jobs_list2])
    # __val/= np.maximum(__val1, __val2)
    # __val = np.concatenate((__val,__val))

    val = np.concatenate((val,val))
    # print('val :', len(val))
    ind_min = 0
    for jobs in jobs_list1 + jobs_list2:
        length = len(jobs)
        qt = np.array(jobs.submission_time)
        cut_min = np.percentile(qt, 100 * dropout_ratio[0])
        cut_max = np.percentile(qt, 100 * dropout_ratio[1])
        dropout_cond = np.logical_and(qt >= cut_min, qt <= cut_max)
        jobs = jobs[dropout_cond]
        
        # print('ind_min :', ind_min, ', length :', length)
        
        for i in range(len(bins) - 1):
            
            batch_val = val[ind_min:ind_min+length]
            batch_val = batch_val[dropout_cond]
            # print('     batch_val', len(batch_val))
            # print('     dropout_cond', len(dropout_cond))

            # __batch_val = __val[ind_min:ind_min+length] # pour ne l'afficher que pour certaines valeurs du rt/wt
            # __batch_val = __batch_val[dropout_cond]
            
            cond = np.logical_and(batch_val>bins[i], batch_val<=bins[i+1])
            # cond = np.logical_and(cond, __batch_val > 0.8) # pour ne l'afficher que pour certaines valeurs du rt/wt

            group_job = jobs[cond]
            # print('     group_job', len(group_job))

            res['nb_of_jobs', mean_values[i]].append(len(group_job))

            res['nb_rejected', mean_values[i]].append(np.array(group_job.final_state == 'REJECTED').sum())
            
            res['nb_killed', mean_values[i]].append(np.array(group_job.final_state == 'COMPLETED_WALLTIME_REACHED').sum())

            res['nb_completed', mean_values[i]].append(np.array(group_job.final_state == 'COMPLETED_SUCCESSFULLY').sum())

            res['raw response time', mean_values[i]].append(user_loss_with_groups(group_job, weight_for_size = False, use_asked_workload = False))

            res['response time (weighted by asked workload)', mean_values[i]].append(user_loss_with_groups(group_job, weight_for_size = True, use_asked_workload = True))

            res['response time (weighted by area)', mean_values[i]].append(user_loss_with_groups(group_job, weight_for_size = True, use_asked_workload = False))

            res['raw bounded slowdown', mean_values[i]].append(bounded_slowdown_with_groups(group_job, weight_for_size = False, use_asked_workload = False))

            res['bounded slowdown (weighted by asked workload)', mean_values[i]].append(bounded_slowdown_with_groups(group_job, weight_for_size = True, use_asked_workload = True))

            res['bounded slowdown (weighted by area)', mean_values[i]].append(bounded_slowdown_with_groups(group_job, weight_for_size = True, use_asked_workload = False))
        ind_min+=length

    return res, bins, mean_values

# old version
def make_group_loss(jobs_list, criterion, bins = None, dropout_ratio = [0.15,0.85]):
    jobs_list = [clear_useless_jobs(jobs) for jobs in jobs_list]
    if criterion == 'nb res':
        val = np.concatenate([np.array(jobs.requested_number_of_resources) for jobs in jobs_list])
    elif criterion == 'requested time':
        val = np.concatenate([np.array(jobs.requested_time) for jobs in jobs_list])
    elif criterion == 'execution time':
        val = np.concatenate([np.array(jobs.execution_time) for jobs in jobs_list])
    elif criterion == 'asked workload':
        val = np.concatenate([np.array(jobs.requested_number_of_resources) for jobs in jobs_list]) * np.concatenate([np.array(jobs.requested_time) for jobs in jobs_list])
    elif criterion == 'real worload':
        val = np.concatenate([np.array(jobs.requested_number_of_resources) for jobs in jobs_list]) * np.concatenate([np.array(jobs.execution_time) for jobs in jobs_list])
    else:
        print('erreur 63 !!!')
        raise 'erreur 63 !!!'

    if bins == None:
        bins = np.linspace(np.min(val), np.max(val), 12)
        mean_values = (bins[1:] + bins[:-1])/2
    elif bins == 'lin':
        bins = np.linspace(np.min(val), np.max(val), 12)
        mean_values = (bins[1:] + bins[:-1])/2
    elif bins == 'pow2':
        bins = np.round(np.geomspace(16, 2**17, 14) * 0.9, 2)
        mean_values = np.array(np.geomspace(16, 2**16, 13), dtype = int)
    elif bins == 'geom':
        bins = np.geomspace(np.min(val), np.max(val), 12)
        mean_values = np.round(np.sqrt(bins[:-1]*bins[1:]), 0)       
    else:
        bins = np.array(bins)
        mean_values = np.array(mean_values)

    loss_list = ['nb_of_jobs', 'nb_rejected', 'nb_killed', 'nb_completed', 'duration', 'raw response time', 'response time (weighted by asked workload)', 'response time (weighted by area)', 'raw bounded slowdown', 'bounded slowdown (weighted by asked workload)', 'bounded slowdown (weighted by area)']

    res = {}
    for loss_name in loss_list:
        for mean_value in mean_values:
            res[loss_name, mean_value] = []


    for jobs in jobs_list:
        qt = np.array(jobs.submission_time)
        cut_min = np.percentile(qt, 100 * dropout_ratio[0])
        cut_max = np.percentile(qt, 100 * dropout_ratio[1])
        cond = np.logical_and(qt >= cut_min, qt <= cut_max)
        jobs = jobs[cond]

        for i in range(len(bins)):

            if criterion == 'nb res':
                val = np.array(jobs.requested_number_of_resources)
            elif criterion == 'requested time':
                val = np.array(jobs.requested_time)
            elif criterion == 'execution time':
                val = np.array(jobs.execution_time)
            elif criterion == 'asked workload':
                val = np.array(jobs.requested_number_of_resources)*np.array(jobs.requested_time)
            elif criterion == 'real worload':
                val = np.array(jobs.requested_number_of_resources)*np.array(jobs.execution_time)
            else:
                print('erreur 63 !!!')
                raise 'erreur 63 !!!'

        for i in range(len(bins) - 1):
            cond = np.logical_and(val>bins[i], val<=bins[i+1])
            group_job = jobs[cond]

            res['nb_of_jobs', mean_values[i]].append(len(group_job))

            res['nb_rejected', mean_values[i]].append(np.array(group_job.final_state == 'REJECTED').sum())
            
            res['nb_killed', mean_values[i]].append(np.array(group_job.final_state == 'COMPLETED_WALLTIME_REACHED').sum())

            res['nb_completed', mean_values[i]].append(np.array(group_job.final_state == 'COMPLETED_SUCCESSFULLY').sum())

            res['raw response time', mean_values[i]].append(user_loss_with_groups(group_job, weight_for_size = False, use_asked_workload = False))

            res['response time (weighted by asked workload)', mean_values[i]].append(user_loss_with_groups(group_job, weight_for_size = True, use_asked_workload = True))

            res['response time (weighted by area)', mean_values[i]].append(user_loss_with_groups(group_job, weight_for_size = True, use_asked_workload = False))

            res['raw bounded slowdown', mean_values[i]].append(bounded_slowdown_with_groups(group_job, weight_for_size = False, use_asked_workload = False))

            res['bounded slowdown (weighted by asked workload)', mean_values[i]].append(bounded_slowdown_with_groups(group_job, weight_for_size = True, use_asked_workload = True))

            res['bounded slowdown (weighted by area)', mean_values[i]].append(bounded_slowdown_with_groups(group_job, weight_for_size = True, use_asked_workload = False))

    return res, bins, mean_values

# ============================================================================
def weighted_percentile(data, percents, weights=None):
    ''' percents in units of 1%
        weights specifies the frequency (count) of data.
    '''
    if weights is None:
        return np.percentile(data, percents)
    ind=np.argsort(data)
    d=data[ind]
    w=weights[ind]
    p=1.*w.cumsum()/w.sum()*100
    y=np.interp(percents, p, d)
    return y

def weighted_quantiles(values, weights, quantiles=0.5):
    i = np.argsort(values)
    c = np.cumsum(weights[i])
    return values[i[np.searchsorted(c, np.array(quantiles) * c[-1])]]

def plot_behaviour_per_group(name_list, loss_per_group_list_list, criterion_list, scales, xlabel_list = None, keyword = 'raw response time', serie_list = ['adjusted_walltime', 'runtime'], number_of_colums = 2, title_size = 15, label_size = 15, show_title = True, fontsize = 15, title_keyword = None):
        if title_keyword == None:
            title_keyword = keyword
        if xlabel_list == None:
            xlabel_list = criterion_list
        for i, (groups_loss_list, bins, mean_values) in enumerate(loss_per_group_list_list):
            plt.subplot( int(np.ceil(len(criterion_list)/number_of_colums)), number_of_colums, i+1)
            ll = []
            # ll2 = []
            ll3 = []
            ll4 = []
            gl = []
            nb_elts_list = []
            for group in mean_values:
                str_ = 'groupe : ' + str(group) + ' ;'
                elts = [-1, -1]
                loss = [-1, -1]
                for j,name in enumerate(serie_list):
                    cond = np.array(name_list) == name
                    elts[j] = np.array(groups_loss_list['nb_of_jobs', group])[cond]
                    loss[j] =  np.array(groups_loss_list[keyword, group])[cond]

                nan_loss = []
                for j in range(2):
                    nan_loss.append(loss[j])
                    nan_loss[j][elts[j]==0] = 0
                # a0 = np.average(nan_loss[0], weights=elts[0]+1e-3)
                # a1 = np.average(nan_loss[1], weights=elts[1]+1e-3)

                a0 = np.array(nan_loss[0])
                a1 = np.array(nan_loss[1])

                gl.append(group)
                
                # ll.append(100*((a0 - a1)/a0))
                ri = 100*((a0 - a1)/(a0+1e-5))
                ri[elts[0]==0] = 0
                # ll.append(np.average(ri, weights=elts[0]+1e-5)) 
                
                ll.append(weighted_quantiles(ri, (elts[0]>0)+1e-6, 0.5)) 
                ll3.append(weighted_quantiles(ri, (elts[0]>0)+1e-6, 0.1))
                ll4.append(weighted_quantiles(ri, (elts[0]>0)+1e-6, 0.9))

              
                # ll3.append(100*np.nanpercentile((loss[0] - loss[1])/loss[0], 10))
                # ll4.append(100*np.nanpercentile((loss[0] - loss[1])/loss[0], 90))
                nb_elts_list.append(elts[0].sum())

            if scales[i] == 'log':
                plt.xscale('log')

            display_cond = np.array(nb_elts_list) > 50 # Pour ne pas afficher les groupes avec trop peu d'elements
            ind_min = np.argmax(display_cond)
            ind_max = len(display_cond) - np.argmax(display_cond[::-1])
            # print(ind_min, ind_max)
            plt.plot(gl[ind_min:ind_max], ll[ind_min:ind_max], label= 'Median')
            # plt.plot(gl, ll2, c = '#2ca02c')
            plt.plot(gl[ind_min:ind_max], ll3[ind_min:ind_max], c = '#2ca02c', linestyle = '--')
            plt.plot(gl[ind_min:ind_max], ll4[ind_min:ind_max], c = '#2ca02c', linestyle = '--', label= '10th and 90th percentile')            
            if scales[i] == 'log':
                plt.plot([gl[ind_min]/1.5, gl[ind_max-1]*1.5], [0,0], c = '#ff7f0e', linestyle = '--')
                plt.xlim(gl[ind_min]/1.5, gl[ind_max-1]*1.5)
            else:
                plt.xlim(0,1)
                plt.plot([0,1], [0,0], c = '#ff7f0e', linestyle = '--')
            if show_title:
                plt.title('Evolution of ' + title_keyword + ' for tasks of similar ' + xlabel_list[i], fontsize = title_size)
            plt.minorticks_on()
            plt.grid(which='major', color = 'black')
            plt.grid(which='minor')
            plt.ylabel('Relative Improvement (%)', fontsize = label_size)
            plt.xlabel(xlabel_list[i], fontsize = label_size)
            for a,b,c in zip(gl[ind_min:ind_max], ll[ind_min:ind_max], nb_elts_list[ind_min:ind_max]):
                plt.text(a,b,c, fontsize = fontsize)

            ax = plt.gca()
            ymin, ymax = ax.get_ylim()
            plt.ylim( max(ymin,-100), min(200, ymax) )
            plt.legend(fontsize = label_size)
        plt.show()

def print_behaviour_per_group(name_list,groups_loss_list, bins, mean_values,  keyword = 'raw response time', serie_list = ['adjusted_walltime', 'runtime']):
    for group in mean_values:
        str_ = 'groupe : ' + str(group) + ' ;'
        elts = [-1, -1]
        loss = [-1, -1]
        for i,name in enumerate(serie_list):
            cond = np.array(name_list) == name
            ind_min = np.argmax(cond)
            ind_max = len(cond) - np.argmax(cond[::-1])
            elts[i] = np.array(groups_loss_list['nb_of_jobs', group][ind_min:ind_max])
            loss[i] =  np.array(groups_loss_list[keyword, group][ind_min:ind_max])
        a0 = np.average(loss[0], weights=elts[0])
        a1 = np.average(loss[1], weights=elts[1])
        print(str_, 'elts :', elts[0].sum(), '; gain :', np.round(100*((a1 - a0)/a1), 2), '%')

def plot_loss_ratio(loss_list, name_list, keywords = ['raw utilization', 'raw bounded slowdown', 'raw response time', 'response time (weighted by area)', 'response time (weighted by number of cores)', 'response time (weighted by execution time)'], names = ['adjusted_walltime', 'runtime'], label_names = None, number_of_colums = 3, title_size = 15, show_title = True):

    for i,keyword in enumerate(keywords):
        clean_name_list = name_list.copy()
        if label_names != None:
            clean_name_list[name_list == names[0]] = label_names[0]
            clean_name_list[name_list == names[1]] = label_names[1]
        plt.subplot( int(np.ceil(len(keywords)/number_of_colums)), number_of_colums, i+1)
        xxx = clean_name_list
        ax = sns.boxplot(x=xxx, y=loss_list[keyword], showfliers = False)
        ax = sns.swarmplot(x=xxx, y=loss_list[keyword], color=".25", size=10)
        if show_title:
            l1 = np.array(loss_list[keyword])[name_list == names[0]]
            l2 = np.array(loss_list[keyword])[name_list == names[1]]
            m1 = np.mean(l1)
            m2 = np.mean(l2)
            gom = str(np.round(100*((m2 - m1)/m1), 2)) + ' %'
            mg = str(np.round(100*np.mean((l2 - l1)/l1), 2)) + ' %'
            std = str(np.round(100*np.std((l2 - l1)/l1), 5)) + ' %'


            if keyword == 'raw utilization' or keyword == 'useful utilization':
                            s1 = str(np.round(m1, 4))
                            s2 = str(np.round(m2, 4))
            else:
                    s1 = str(np.round(m1, 0))
                    s2 = str(np.round(m2, 0))
            plt.title(keyword +  ' ' + s1 + ' ' + s2 + ' : gain on mean ' + gom + ',\n' + 'mean gain ' + mg + ',  std of ' + std, fontsize = title_size)
            plt.title(keyword + ' : gain on mean ' + gom + ',\n' + 'mean gain ' + mg + ',  std of ' + std, fontsize = title_size)
    plt.show()

def plot_loss_ratio_multi(loss_list, name_list, keywords = ['raw utilization', 'raw bounded slowdown', 'raw response time', 'response time (weighted by area)', 'response time (weighted by number of cores)', 'response time (weighted by execution time)'], names = ['adjusted_walltime', 'runtime'], label_names = None, number_of_colums = 3, title_size = 12, show_title = True):

    for i,keyword in enumerate(keywords):
        clean_name_list = name_list.copy()
        if label_names is None:
            for j in range(len(label_names)):
                clean_name_list[name_list == names[j]] = label_names[j]

        plt.subplot( int(np.ceil(len(keywords)/number_of_colums)), number_of_colums, i+1)
        xxx = clean_name_list
        if show_title:
            l1 = np.array(loss_list[keyword])[name_list == names[0]]
            m1 = np.mean(l1)
            if keyword == 'raw utilization' or keyword == 'useful utilization' or keyword == 'utilization_std':
                s1 = str(np.round(m1, 4))
            else:
                s1 = str(np.round(m1, 0))
            s_list = [s1]
            gom_list = []
            mg_list = []
            std_list = []
            for k in range(1,len(names)):
                l2 = np.array(loss_list[keyword])[name_list == names[k]]
                m2 = np.mean(l2)
                gom = str(np.round(100*((m2 - m1)/m1), 2)) + ' %'
                mg = str(np.round(100*np.mean((l2 - l1)/l1), 2)) + ' %'
                std = str(np.round(100*np.std((l2 - l1)/l1), 5)) + ' %'

                gom_list.append(gom)
                mg_list.append(gom)
                std_list.append(std)
                if keyword == 'raw utilization' or keyword == 'useful utilization':
                    s2 = str(np.round(m2, 4))
                else:
                    s2 = str(np.round(m2, 0))
                s_list.append(s2)
                plt.title(keyword +  ' : ' + str(s_list)  + '\ngain on mean : ' + str(gom_list) +  '\nstd : ' + str(std_list), fontsize = title_size)

        ax = sns.boxplot(x=xxx, y=loss_list[keyword], showfliers = False)
        ax = sns.swarmplot(x=xxx, y=loss_list[keyword], color=".25", size=10)
            
    plt.show()

def plot_loss_ratio_bis_multi(loss_list, name_list, keywords = ['raw utilization', 'raw bounded slowdown', 'raw response time', 'response time (weighted by area)', 'response time (weighted by number of cores)', 'response time (weighted by execution time)'], names = ['adjusted_walltime', 'runtime'], label_names = None, benchmark_name = 'adjusted_walltime', absciss_setter_name = 'runtime', plot = False, number_of_colums = 3, title_size = 12, show_title = True):

    pos_of_benchmark = name_list == benchmark_name
    pos_of_abscisse_setter = name_list == absciss_setter_name

    for i,keyword in enumerate(keywords):
        clean_name_list = name_list.copy()
        if label_names is None:
            for j in range(len(label_names)):
                clean_name_list[name_list == names[j]] = label_names[j]

        bench_perf = np.array(loss_list[keyword])[pos_of_benchmark]
        absciss_perf =  np.array(loss_list[keyword])[pos_of_abscisse_setter]

        plt.subplot( int(np.ceil(len(keywords)/number_of_colums)), number_of_colums, i+1)
        xxx = clean_name_list
        if show_title:
            l1 = np.array(loss_list[keyword])[name_list == names[0]]
            m1 = np.mean(l1)
            if keyword == 'raw utilization' or keyword == 'useful utilization':
                s1 = str(np.round(m1, 4))
            else:
                s1 = str(np.round(m1, 0))
            s_list = [s1]
            gom_list = []
            mg_list = []
            std_list = []
        for k in range(1,len(names)):
            if show_title:
                l2 = np.array(loss_list[keyword])[name_list == names[k]]
                m2 = np.mean(l2)
                gom = str(np.round(100*((m2 - m1)/m1), 2)) + ' %'
                mg = str(np.round(100*np.mean((l2 - l1)/l1), 2)) + ' %'
                std = str(np.round(100*np.std((l2 - l1)/l1), 5)) + ' %'
                gom_list.append(gom)
                mg_list.append(gom)
                std_list.append(std)
                if keyword == 'raw utilization' or keyword == 'useful utilization':
                    s2 = str(np.round(m2, 4))
                else:
                    s2 = str(np.round(m2, 0))
                s_list.append(s2)
                
            #order = np.argsort(absciss_perf)
            if plot:
                order = np.argsort(absciss_perf)
                plt.plot(absciss_perf[order],  ((np.array(loss_list[keyword])[name_list == names[k]] - bench_perf)/bench_perf)[order], label = label_names[k])
            else:
                plt.scatter(absciss_perf,  ((np.array(loss_list[keyword])[name_list == names[k]] - bench_perf)/bench_perf), label = label_names[k])
            if show_title:
                plt.title(keyword +  ' : ' + str(s_list)  + '\ngain on mean : ' + str(gom_list) +  '\nstd : ' + str(std_list), fontsize = title_size)
            # plt.xlabel(keyword)
            # plt.ylabel('Relative improvement')
        #plt.legend()
    plt.show()


def is_backfilled_list(jobs_ini, total_nb_nodes = 4392, dropout_ratio = [0.15,0.85], warning = True):
    jobs = jobs_ini

    jobs = clear_useless_jobs(jobs)
    qt = np.array(jobs.submission_time)
    cut_min = np.percentile(qt, 100 * dropout_ratio[0])
    cut_max = np.percentile(qt, 100 * dropout_ratio[1])
    cond = np.logical_and(qt >= cut_min, qt <= cut_max)
    jobs = jobs[cond]
    jobs = jobs.sort_values(by=['starting_time'])

    ar = np.array(jobs.allocated_resources)

    jobs_ids = np.array(jobs.job_id)
    jobs_ids = np.array([ int(e.split('_')[0]) for e in jobs_ids])

    list_per_node = [ [] for _ in range(total_nb_nodes)]
    for i,s in enumerate(ar):
        component_list = s.split(' ')
        for j,r in enumerate(component_list):
            split = r.split('-')
            ind_min = int(split[0])
            ind_max = int(split[-1]) + 1
        
            for k in range(ind_min, ind_max):
                list_per_node[k].append(jobs_ids[i])

    is_backfilled = -1*np.ones(len(jobs_ini))
    for i,list_ in enumerate(list_per_node):
        current_min = np.inf
        for j,job_id in reversed(list(enumerate(list_))):
            if job_id < current_min:
                current_min = job_id
                is_backfilled[job_id] = 0
            elif job_id > current_min:
                is_backfilled[job_id] = 1
            else:
                print('bug n°88 !!')
                raise 'beug n°88'
    if warning:
        print('Attention : les indices de la sortie correpondent aux job_id, pas à l\'ordre dans l\'input !!')
    return is_backfilled


def is_backfilled_dfs(jobs_ini, total_nb_nodes = 4392, dropout_ratio = [0.15,0.85]):
    jobs = jobs_ini

    jobs = clear_useless_jobs(jobs)
    qt = np.array(jobs.submission_time)
    cut_min = np.percentile(qt, 100 * dropout_ratio[0])
    cut_max = np.percentile(qt, 100 * dropout_ratio[1])
    cond = np.logical_and(qt >= cut_min, qt <= cut_max)
    jobs = jobs[cond]
    jobs = jobs.sort_values(by=['starting_time'])

    ar = np.array(jobs.allocated_resources)

    jobs_ids = np.array(jobs.job_id)
    jobs_ids = np.array([ int(e.split('_')[0]) for e in jobs_ids])

    list_per_node = [ [] for _ in range(total_nb_nodes)]
    for i,s in enumerate(ar):
        component_list = s.split(' ')
        for j,r in enumerate(component_list):
            split = r.split('-')
            ind_min = int(split[0])
            ind_max = int(split[-1]) + 1
        
            for k in range(ind_min, ind_max):
                list_per_node[k].append(jobs_ids[i])

    res = -1*np.ones(len(jobs_ini))
    for i,list_ in enumerate(list_per_node):
        current_min = np.inf
        for j,job_id in reversed(list(enumerate(list_))):
            if job_id < current_min:
                current_min = job_id
                res[job_id] = 0
            elif job_id > current_min:
                res[job_id] = 1
            else:
                print('bug n°88 !!')
                raise 'beug n°88'

    jobs = jobs_ini.sort_values(by=['job_id'], key = lambda x : np.array([ int(e.split('_')[0]) for e in np.array(x)]) )
    is_backfilled = jobs[res == 1]
    not_backfilled = jobs[res == 0]

    return is_backfilled, not_backfilled

#=================================================================================================
# Analyis sythetic traces

def plot_series(jl, x, loss_list, name_list, keyword, accepted_names, label_names = None, dropout_ratio = [0.15,0.85], logscale = True, fontsize = 20, show_title = True):
    if label_names is None:
        label_names = accepted_names

    y_min = np.inf
    for i,name in enumerate(accepted_names):
        cond = np.array(name_list) == name
        ind_min = np.argmax(cond)
        ind_max = len(cond) - np.argmax(cond[::-1])
        y_min_ = np.amin(loss_list[keyword][ind_min:ind_max])
        y_min = min(y_min_, y_min)

    for i,name in enumerate(accepted_names):
        cond = np.array(name_list) == name
        ind_min = np.argmax(cond)
        ind_max = len(cond) - np.argmax(cond[::-1])

        df = jl[ind_min]
        df = clear_useless_jobs(df)
        qt = np.array(df.submission_time)
        cut_min = np.percentile(qt, 100 * dropout_ratio[0])
        cut_max = np.percentile(qt, 100 * dropout_ratio[1])
        cond_ = np.logical_and(qt >= cut_min, qt <= cut_max)   
        df = df[cond_]
        cm = np.array(df.requested_number_of_resources)
        rtm = np.array(df.execution_time)
        cste = 49152/(cm*rtm).mean()
        # cm = np.array(df.requested_number_of_resources).mean()
        # rtm = np.array(df.execution_time).mean()
        # cste = 49152/(cm*rtm)
        steady_state = np.array(loss_list['useful utilization'][ind_min:ind_max])*cste > x[ind_min:ind_max]/3600 # /1000 because we have multiply x axes by 1000
        # y_min = np.amin(loss_list[keyword])
        # print(steady_state)

        xx = np.array(x, dtype = np.float32)[cond]
        ll = np.array(loss_list[keyword][ind_min:ind_max])
        # ss_ = steady_state[ind_min:ind_max]
        
        ss_ = np.logical_not(steady_state)#np.logical_not(ss_)
        ind_lim = np.argmax(ss_) - 1
        plt.plot(xx[:ind_lim+1], ll[:ind_lim+1], label = label_names[i])
        ss_[ind_lim] = True
        plt.plot(xx[ss_], ll[ss_], '--', c = 'red')  
        # print(steady_state)
        if ind_lim > 0:
            # print(ind_min, ind_lim, ind_max)
            # if ind_lim >= ind_min and ind_lim <= ind_max:
            plt.scatter([xx[ind_lim]], loss_list[keyword][ind_min+ind_lim], marker = 'X', c='red', s = 200, linewidths = 1)
            plt.plot([x[ind_min+ind_lim]]*2, [y_min, loss_list[keyword][ind_min+ind_lim]], '--', c='green' )
    if logscale:
        plt.xscale('log')
    plt.legend(fontsize = fontsize)
    if show_title:
        plt.title(keyword, size = fontsize)
    plt.show()

def plot_series_ratio(jl, x, loss_list, name_list, keyword, accepted_names,  label_names = None, dropout_ratio = [0.15,0.85], logscale = True, show_title = True,fontsize = 20):
    
    cond1 = np.array(name_list) == accepted_names[0]
    ind_min1 = np.argmax(cond1)
    ind_max1 = len(cond1) - np.argmax(cond1[::-1])
    df = jl[np.argmax(cond1)]
    df = clear_useless_jobs(df)
    qt = np.array(df.submission_time)
    cut_min = np.percentile(qt, 100 * dropout_ratio[0])
    cut_max = np.percentile(qt, 100 * dropout_ratio[1])
    cond = np.logical_and(qt >= cut_min, qt <= cut_max)   
    df = df[cond]
    cm = np.array(df.requested_number_of_resources)
    rtm = np.array(df.execution_time)
    cste = 49152/(cm*rtm).mean()
    # cm = np.array(df.requested_number_of_resources).mean()
    # rtm = np.array(df.execution_time).mean()
    # cste = 49152/(cm*rtm)
    steady_state1 = np.array(loss_list['useful utilization'][ind_min1:ind_max1])*cste > x[ind_min1:ind_max1]/3600 # /1000 because we have multiply x axes by 1000
    ss1_ = np.logical_not(steady_state1)
    ind_lim1_ = np.argmax(ss1_) - 1

    yy_lim1_list = []

    for i in range(1, len(accepted_names)): 
        
        cond2 = np.array(name_list) == accepted_names[i]
        ind_min2 = np.argmax(cond2)
        ind_max2 = len(cond2) - np.argmax(cond2[::-1])
        df = jl[np.argmax(cond2)]
        df = clear_useless_jobs(df)
        qt = np.array(df.submission_time)
        cut_min = np.percentile(qt, 100 * dropout_ratio[0])
        cut_max = np.percentile(qt, 100 * dropout_ratio[1])
        cond = np.logical_and(qt >= cut_min, qt <= cut_max)   
        df = df[cond]
        cm = np.array(df.requested_number_of_resources)
        rtm = np.array(df.execution_time)
        cste = 49152/(cm*rtm).mean()
        # cm = np.array(df.requested_number_of_resources).mean()
        # rtm = np.array(df.execution_time).mean()
        # cste = 49152/(cm*rtm)
        steady_state2 = np.array(loss_list['useful utilization'][ind_min2:ind_max2])*cste > x[ind_min2:ind_max2]/3600 # /1000 because we have multiply x axes by 1000
        yy = np.array(loss_list[keyword][ind_min1:ind_max1])/np.array(loss_list[keyword][ind_min2:ind_max2])

        yy_lim1_list.append(yy[ind_lim1_])
        # plt.plot([x[ind_min+ind_lim]]*2, [np.amin(yy), yy[ind_min+ind_lim]], '--', c='green' )
        plt.scatter([x[ind_lim1_]], yy[ind_lim1_], marker = 'X', c='red', s = 200, linewidths = 1)
        ss2_ = np.logical_not(steady_state2)
        ind_lim2_ = np.argmax(ss2_) - 1
        plt.scatter([x[ind_lim2_]], yy[ind_lim2_], marker = 'X', c='red', s = 200, linewidths = 1)
        # plt.plot([x[ind_min+ind_lim]]*2, [np.amin(yy), yy[ind_min+ind_lim]], '--', c='green' )

        # part1 = np.logical_and(np.logical_not(ss1_), np.logical_not(ss2_))
        # part2 = np.logical_xor(np.logical_not(ss1_), np.logical_not(ss2_))
        # part2[np.argmax(part2) - 1] = True
        # part3 = np.logical_and(ss1_, ss2_)
        # part3[np.argmax(part3) - 1] = True

        ind_lim1 = min(ind_lim1_,ind_lim2_)
        ind_lim2 = max(ind_lim1_,ind_lim2_)
        if label_names is not None:
            plt.plot(np.array(x[cond1], dtype = np.float32)[:ind_lim1+1], yy[:ind_lim1+1], label = label_names[i-1])
        else:
            plt.plot(np.array(x[cond1], dtype = np.float32)[:ind_lim1+1], yy[:ind_lim1+1])

        plt.plot(np.array(x[cond1], dtype = np.float32)[ind_lim1:ind_lim2+1], yy[ind_lim1:ind_lim2+1], '--',  c='orange')
        # plt.plot(np.array(x[cond1], dtype = np.float32)[part3], yy[part3], '--', c='r')
        if label_names is not None:
            plt.legend(fontsize = fontsize)
        if logscale:
            plt.xscale('log')
        if show_title:
            plt.title('ratio of \'' + keyword + '\' for strategies \'' + accepted_names[0] + '\' and \'' + accepted_names[i] + '\'', size = fontsize)
    if len(accepted_names) > 2:
        m, M = np.amin(yy_lim1_list),  np.amax(yy_lim1_list)
        plt.plot([x[ind_min1+ind_lim1_]]*2, [m-(M-m)*0.15, M+(M-m)*0.15], '--', c='green' )
    else:
        plt.scatter(x[ind_min1+ind_lim1_], yy_lim1_list[0], s = 1000, marker = '|', c='green' )

def number_of_cores_used(df):
    start = np.array(df.starting_time)
    end = np.array(df.finish_time)
    cores_requested = np.array(df.requested_number_of_resources)
    start_arg = np.argsort(np.array(start))
    end_arg = np.argsort(np.array(end))

    si, ei = 0, 0
    times, cores_used = [min(start)], [0]

    while max(si, ei) < len(df):
        if start[start_arg[si]] < end[end_arg[ei]]:
            times.append(start[start_arg[si]])
            cores_used+= [cores_used[-1] + cores_requested[start_arg[si]]]
            si += 1
        else:
            times.append(end[end_arg[ei]])
            cores_used+= [cores_used[-1] - cores_requested[end_arg[ei]]]
            ei += 1
    while si < len(df):
            times.append(start[start_arg[si]])
            cores_used+= [cores_used[-1] + cores_requested[start_arg[si]]]
            si += 1
    while ei < len(df):
            times.append(end[end_arg[ei]])
            cores_used+= [cores_used[-1] - cores_requested[end_arg[ei]]]
            ei += 1 
    return np.array(times), np.array(cores_used)

def workload_in_queue(df):
    queue = np.array(df.submission_time)
    start = np.array(df.starting_time)
    rt = np.array(df.execution_time)
    cores_requested = np.array(df.requested_number_of_resources)
    queue_arg = np.argsort(queue)
    start_arg = np.argsort(start)

    qi, si = 0, 0
    times, queue_work = [min(df.submission_time)], [0]

    while max(qi, si) < len(df):
        if queue[queue_arg[qi]] <= start[start_arg[si]]:
            times.append(queue[queue_arg[qi]])
            queue_work+= [queue_work[-1] + rt[queue_arg[qi]] * cores_requested[queue_arg[qi]]]
            qi += 1
        else:
            times.append(start[start_arg[si]])
            queue_work+= [queue_work[-1] - rt[start_arg[si]] * cores_requested[start_arg[si]]]
            si += 1
    while qi < len(df):
            times.append(queue[queue_arg[qi]])
            queue_work+= [queue_work[-1] + rt[queue_arg[qi]] * cores_requested[queue_arg[qi]]]
            qi += 1
    while si < len(df):
            times.append(start[start_arg[si]])
            queue_work+= [queue_work[-1] - rt[start_arg[si]] * cores_requested[start_arg[si]]]
            si += 1 
    return np.array(times), np.array(queue_work)



def plot_utilization_density(job_list, label_list = None): # !!! buguée et pas à jour !!!

    for i,jl in enumerate(job_list):
        t,c = number_of_cores_used(jl)
        hist, bins = np.histogram(c[:-1], weights = t[1:] - t[:-1], bins = 10000)
        
        normalization_factor = np.sum(hist)
        if label_list is None:
            plt.plot( (bins[1:]+bins[:-1])/2, hist/normalization_factor)
        else:
            plt.plot( (bins[1:]+bins[:-1])/2, hist/normalization_factor, label = label_list[i])
    plt.xlabel('number of cores used')
    plt.ylabel('density')
    if label_list is not None:
        plt.legend(fontsize = 28)

def plot_cumulative_utilization(job_list, label_list = None, fontsize = 28, show_label = True, show_title = False):

    for i,jl in enumerate(job_list):
        t,c = number_of_cores_used(jl)
        hist, bins = np.histogram(c[:-1], weights = t[1:] - t[:-1], bins = 10000)
        
        cumsum = np.cumsum(hist)
        normalization_factor = cumsum[-1]
        if label_list is None:
            plt.plot( (bins[1:]+bins[:-1])/2, cumsum/normalization_factor)
        else:
            plt.plot( (bins[1:]+bins[:-1])/2, cumsum/normalization_factor, label = label_list[i])
    if show_title:
        ll = [np.round(utilization_with_dropout(jl, dropout_ratio = [0.15,0.85], total_number_of_nodes=49152),7) for jl in job_list]
        plt.title('mean utilization : ' + str(ll) + '\n' + 'relative improvement : ' + str(100*(ll[1]-ll[0])/ll[0]))
    if show_label:
        plt.xlabel('number of cores used')
        plt.ylabel('cumulative probability')
    if label_list is not None:
        plt.legend(fontsize = fontsize)

def plot_workload_in_queue(job_list, label_list = None, fontsize = 28, show_label = True):

    for i,jl in enumerate(job_list):
        t,c = workload_in_queue(jl)
        repeat_moments_array = np.repeat(t, 2)[1:]
        repeat_cores_used_array = np.repeat(c, 2)[:-1]
        
        if label_list is None:

            plt.plot((repeat_moments_array - np.min(repeat_moments_array))/(3600*24), repeat_cores_used_array)
        else:
            plt.plot((repeat_moments_array - np.min(repeat_moments_array))/(3600*24), repeat_cores_used_array, label = label_list[i])
    if show_label:
        plt.ylabel('workload in queue')
        plt.xlabel("Time (in days)")
    if label_list is not None:
        plt.legend(fontsize = 28)

def work_done(df):
    df = df.sort_values(['finish_time'])
    end = np.array(df.finish_time)
    rt = np.array(df.execution_time)
    cores_requested = np.array(df.requested_number_of_resources)

    times, work_done = [min(df.starting_time)], [0]

    for i, fin_t in enumerate(end):
        times.append(fin_t)
        work_done.append(work_done[-1] + rt[i]*cores_requested[i])

    return np.array(times), np.array(work_done)

def plot_work_done(job_list, time_normalization = False, Cores_normalization = None, label_list = None):

    for i,jl in enumerate(job_list):
        t,w = work_done(jl)
        repeat_moments_array = np.repeat(t, 2)[1:]
        repeat_work_done_array = np.repeat(w, 2)[:-1]

        normalization_const = np.ones(len(repeat_work_done_array))
        if time_normalization:
            normalization_const = repeat_moments_array - repeat_moments_array[0]
        if Cores_normalization is not None:
            normalization_const*= Cores_normalization
        
        if label_list is None:
            plt.plot((repeat_moments_array - np.min(repeat_moments_array))/(3600*24), repeat_work_done_array/normalization_const)
        else:
            plt.plot((repeat_moments_array - np.min(repeat_moments_array))/(3600*24), repeat_work_done_array/normalization_const, label = label_list[i])
            
def plot_number_of_cores_used(job_list, Cores_normalization = None, label_list = None):

    for i,jl in enumerate(job_list):
        t,u = number_of_cores_used(jl)
        repeat_moments_array = np.repeat(t, 2)[1:]
        repeat_cores_used_array = np.repeat(u, 2)[:-1]

        normalization_const = np.ones(len(repeat_cores_used_array))
        if Cores_normalization is not None:
            normalization_const*= Cores_normalization
        
        if label_list is None:
            plt.plot((repeat_moments_array - np.min(repeat_moments_array))/(3600*24), repeat_cores_used_array/normalization_const)
        else:
            plt.plot((repeat_moments_array - np.min(repeat_moments_array))/(3600*24), repeat_cores_used_array/normalization_const, label = label_list[i])

def loss_ratio_boxplot(loss_list_list, name_list_list, 
    loss_list_names = ['Mira,', 'Theta,'],
    names = ['adjusted_walltime','runtime'],
    keywords = ['raw utilisation', 'utilisation_std', 'response time (weighted by area)','raw response time','raw bounded slowdown'],
    keywords_names = ['Utilization', 'Utilisation\nStandard Deviation', 'Response Time\n(weighted by area)','Response Time\n(uniform weight)','bounded slowdown']
    ):

    xxx = []
    yyy = []
    for i,keyword in enumerate(keywords):
        for j,loss_list in enumerate(loss_list_list):
        
            name_list = name_list_list[j]

            loss = np.array(loss_list[keyword])
            cond1 = name_list == names[0]
            cond2 = name_list == names[1]
            yyy.append(100*(loss[cond2] - loss[cond1])/loss[cond1])

            xxx.append(loss_list_names[j] + '\n' + keywords_names[i])

    plt.boxplot(yyy)
    plt.xticks(range(1, 1+len(yyy)), xxx)

def loss_ratio_boxplot2(loss_list_list, name_list_list, 
    loss_list_names = ['Mira,', 'Theta,'],
    names = ['adjusted_walltime','runtime'],
    keywords = ['raw utilisation', 'utilisation_std', 'response time (weighted by area)','raw response time','raw bounded slowdown'],
    keywords_names = ['Utilization', 'Utilisation\nStandard Deviation', 'Response Time\n(weighted by area)','Response Time\n(uniform weight)','bounded slowdown']):

    xxx = []
    yyy = []
    for i,keyword in enumerate(keywords):
        for j,loss_list in enumerate(loss_list_list):
        
            name_list = name_list_list[j]

            loss = np.array(loss_list[keyword])
            cond1 = name_list == names[0]
            cond2 = name_list == names[1]
            yyy.append(100*(loss[cond2] - loss[cond1])/loss[cond1])

            xxx.append(loss_list_names[j])# + '\n' + keywords_names[i])

    plt.boxplot(yyy)
    plt.xticks(range(1, 1+len(yyy)), xxx)

def loss_ratio_boxplot3(loss_list_list, name_list_list, 
    loss_list_names = ['Mira,', 'Theta,'],
    names = ['adjusted_walltime','runtime'],
    keywords = ['raw utilisation', 'utilisation_std', 'response time (weighted by area)','raw response time','raw bounded slowdown'],
    keywords_names = ['Utilization', 'Utilisation\nStandard Deviation', 'Response Time\n(weighted by area)','Response Time\n(uniform weight)','bounded slowdown']
    ):

    xxx = []
    yyy = []
    for i,keyword in enumerate(keywords):
        for j,loss_list in enumerate(loss_list_list):
        
            name_list = name_list_list[j]

            loss = np.array(loss_list[keyword])
            cond1 = name_list == names[0]
            cond2 = name_list == names[1]
            yyy.append(100*(loss[cond1] - loss[cond2])/loss[cond1])

            xxx.append(loss_list_names[j])# + '\n' + keywords_names[i])

    plt.boxplot(yyy)
    plt.xticks(range(1, 1+len(yyy)), xxx)

def loss_ratio_boxplot4(loss_list, name_list, maximization_objective = False,
    prefixs = ['', '3e6cut'],
    prefixs_label = ['All jobs', '3e6 cut'],
    names = ['adjusted_walltime','runtime'],
    keywords = ['raw response time','raw bounded slowdown'],
    keywords_label = ['Response Time\n(uniform weight)','bounded slowdown']
    ):

    xxx = []
    yyy = []
    for i,keyword in enumerate(keywords):
        for j,prefix in enumerate(prefixs):

            loss = np.array(loss_list[keyword])
            cond1 = name_list == prefix + names[0]
            cond2 = name_list == prefix + names[1]
            if maximization_objective:
                yyy.append(100*(loss[cond2] - loss[cond1])/loss[cond1])
            else:
                yyy.append(100*(loss[cond1] - loss[cond2])/loss[cond1])
            xxx.append(prefixs_label[j] + '\n' + keywords_label[i])

    plt.boxplot(yyy)
    plt.xticks(range(1, 1+len(yyy)), xxx)
    return yyy