 
 Our results can be explored through 3 notebooks :
 
 - First_use-case_results.ipynb
 - RLSched_results.ipynb
 - traces.ipynb
 
 The notebooks can easily be runned in order to navigate through our results. 
 However, if you want to do substantial modifications to their code, you will have 
 to go into our .py files that can be much harder to navigate into.
  
 So, if you have any issue/question, do not hesitate to contact robin-dot-boezennec-at-inria-dot-fr
 
 Note that if you want to re-run our experiments, you will have to install batsim https://batsim.readthedocs.io/en/latest/
 (for the first use-case), or use the RlScheduler code https://github.com/DIR-LAB/deep-batch-scheduler (for the second use-case)
 
===============================================
 - First_use-case_results.ipynb
 
 This Notebook reproduce the figures from our first use-case.
 It uses the traces stored in the folder batsim_outputs
 
 
===============================================
 - RLSched_results.ipynb
 
 This Notebook reproduce the figures from our second use-case.
 It uses the traces stored in the folder RLsched_traces
 
 
 =================================================
 - traces.ipynb
 
 This Notebook contains the code we used to manipulate and analyse the different traces before our experiments.
 This includes parsing .swf traces into the batsim format
 The folders traces and RLsched_traces already contain traces we used but they are not in batsim format.
 
 To run batsim we advise you to consult the official documentation https://batsim.readthedocs.io/en/latest/
 We defined our platform with the file platform.xml
 When using different traces, don't forget to change the size of the platform. It can be done by changing
 the upper bound of 'radical', line 6 in platform.xml
 (the upper bound should be equal to the number of cores/nodes minus 1)
 NB : In the traces and platform, we used the number of nodes and not the number of cores. Mira had node of
 16 cores, so we defined a platform with 49152 cores
 and not 786432. Theta has nodes of 64 cores.
 
 If you want to run our experiments with RLScheduler, you will have to use the author's code https://github.com/DIR-LAB/deep-batch-scheduler
